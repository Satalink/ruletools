/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ruletools;

import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author ag991n
 */
public class Main implements Pkg_Interface{
    private String PROGNAME = "RuleTools";
    private double VERNUM = 0.7;
    private String VERSION = "v"+VERNUM+"";
    protected String dbURL = null;
    protected String dbUser = null;
    protected String dbPass = null;
    protected String OSI_SYSTEM = null;
    protected String svnurl = "svn://subproj1/nms_netx/";
    protected String HOME = System.getProperty("user.home");
    protected String dbDriver = null;
    protected Connection conn = null;

    final String OS = getOSName();
    public Pkg_Interface iface;
    public Properties properties = new Properties();
    public Splash eSplash;
    public GUI eGUI;
    public OraDB eDB;
    public XMLConfig eXML;
    public File config = new File(HOME+File.separator+".ruletools"); 
    
    public static void main(String[] args) throws Exception {
        Main main = new Main();
    }

    public Main() throws Exception {
        eSplash = new Splash(this);
        eXML = new XMLConfig();
        eGUI = new GUI(this);
        setDefaultTitle();
        Image winicon = new ImageIcon(getURL("RuleTools_Icon.png")).getImage();
        eGUI.setIconImage(winicon);
        properties = getProperties();
        getConnected();
        eSplash.killsplash();
        betaExpr(1255700000);
        eGUI.setAlwaysOnTop(true);
        eGUI.setAlwaysOnTop(false);
    }

    private void betaExpr(long expTime) {
        Calendar timestamp = new GregorianCalendar();
        long time = (timestamp.getTimeInMillis() / 1000);
        if(expTime < time) {
            System.out.println("Beta version of RuleTools has expired.");
            System.exit(0);
        }
    }
    public void exitApp() {
        eGUI.setVisible(false);
        eDB.DbClose(conn);
        setConfig();
        System.exit(0);
    }
    public void getConnected() {
        eDB = new OraDB();
        dbDriver = properties.getProperty("dbDriver", "oracle.jdbc.driver.OracleDriver");
        properties.setProperty("dbDriver", dbDriver);
        if(Boolean.valueOf(properties.getProperty("GUI.jCheckBox_loadlastDB", "false"))) { 
           dbURL =  properties.getProperty("dbURL");
           dbUser = properties.getProperty("dbUser");
           dbPass = properties.getProperty("dbPass"); 
           conn = eDB.DbConnect(dbDriver, dbURL, dbUser, dbPass);
            if(getConnStat()) {
                setDefaultTitle();
                eGUI.setTitle(eGUI.getTitle()+"   @"+dbURL+"@"+dbUser);
                eGUI.initReader(properties);
                eGUI.setVisible(true);
            } else {
                GUI_Options options = new GUI_Options(this);
                options.setVisible(true);               
            }       
        } else if(OS.contains("SunOS") &&
           System.getenv("OSI_SYSTEM") != null &&
           System.getenv("OSI_DBPTR") != null &&
           System.getenv("OSI_DB_SERVER") != null &&
           System.getenv("OSI_DB_PORT") != null &&
           System.getenv("ORACLE_SID") != null &&
           System.getenv("OSI_DB_USER") != null &&
           System.getenv("OSI_DB_PASSWORD") != null
           ) {
            dbURL = "jdbc:oracle:thin:@" + System.getenv("OSI_DB_SERVER") + ":" + System.getenv("OSI_DB_PORT") + ":" + System.getenv("ORACLE_SID");
            dbUser = System.getenv("OSI_DB_USER");
            dbPass = System.getenv("OSI_DB_PASSWORD");
            conn = eDB.DbConnect(dbDriver, dbURL, dbUser, dbPass);
            if(getConnStat()) {
                setDefaultTitle();
                eGUI.setTitle(eGUI.getTitle()+"   @"+dbURL+"@"+dbUser);                
                eGUI.initReader(properties);
                eGUI.setVisible(true);
            } else {
                GUI_Options options = new GUI_Options(this);
                options.setVisible(true);                
            }
        } else {
            GUI_Options options = new GUI_Options(this);
            options.setVisible(true);
        }
    }
    public boolean getConnStat() {
        boolean status = false;
        try {
            if(conn != null) status = !conn.isClosed();
        } catch (SQLException ex) {
            status = false;
        }
        return(status);
    }
    public OraDB getDB() {
        return(eDB);
    }   
    public GUI getGUI() {
        return(eGUI);
    }
    public ArrayList getDBquery(String sql) {
        ArrayList results = eDB.DbSelect(sql, conn);
        return(results);
    }
    public String getOSName() {
        return(System.getProperty("os.name"));
    }     
    public String getSVNroot() {
        return(svnurl);
    }
    public URL getURL(String link) {
        String UniRefLink = null;
        JarFile mainjar = null;
        URL refURL = null;
        try {
            mainjar = new JarFile(PROGNAME+".jar");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Enumeration entries = mainjar.entries();
        while (entries.hasMoreElements()) {
            String curURL = entries.nextElement().toString();
            if(curURL.contains(link)) {
            UniRefLink = curURL;
            break;
            }
        }
            try {
            refURL = new URL("jar:file:/"+System.getProperty("user.dir")+System.getProperty("file.separator")+PROGNAME+".jar!/"+UniRefLink);
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            }
        return(refURL);
    }    
    public XMLConfig getConfig() {
        return(eXML);
    }
    public Properties getProperties() throws Exception {        
        if(config.exists() && properties.size() == 0) {
            Properties props = eXML.readProperties(config);
            //Set GUI POSITION
            if(Boolean.valueOf(props.getProperty("GUI.jCheckBox_memGUIpos", "false"))) {
                String[] posStrings = props.getProperty("GUI.POS", "100,100").split(",");
                eGUI.setLocation(Integer.valueOf(posStrings[0]), Integer.valueOf(posStrings[1]));
            }
            //Set GUI DIMENSIONS
            if(Boolean.valueOf(props.getProperty("GUI.jCheckBox_memGUIdim", "false"))) {
                String[] dimStrings = props.getProperty("GUI.DIM", "1012,839").split(",");
                eGUI.setSize(Integer.valueOf(dimStrings[0]), Integer.valueOf(dimStrings[1]));
            }            
            return(props);
        } else {
            return(properties);
        }
    }
    public void setConfig() {
        //GUI Properties
        properties.setProperty("GUI.POS", String.valueOf(eGUI.getLocation().x)+","+String.valueOf(eGUI.getLocation().y));
        properties.setProperty("GUI.DIM", String.valueOf(eGUI.getSize().width)+","+String.valueOf(eGUI.getSize().height));
        properties = eGUI.getGUIprops(properties);
        //Database Properties
        
        //SVN Properties
        properties.setProperty("SVN.URL", svnurl);
        try {
            eXML.writeProperties(properties, config);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    public void setConnection(Connection connection) {
        conn = connection;
    }
    public void setProperties(Properties props) {
        Enumeration e = props.propertyNames();
        while(e.hasMoreElements()) {
            String key = e.nextElement().toString();
            String value = props.getProperty(key);
            properties.setProperty(key, value);
        }
    }
    public void setDefaultTitle() {
        eGUI.setTitle("NetExpert "+PROGNAME+" "+VERSION+"");
    }
}
