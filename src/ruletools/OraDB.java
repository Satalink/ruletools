/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ruletools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ag991n
 */
public class OraDB {
    public Connection DbClose(Connection dbConn){
        try {                        
            if (dbConn != null && !dbConn.isClosed()) {
                dbConn.close();
                dbConn = null;
            }
            else 
                dbConn = null;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return dbConn;
    }    
    public Connection DbConnect(String dbDriver, String dbURL, String dbUser, String dbPassWord){
        Connection dbConn = null;
        try {
            Class.forName(dbDriver).newInstance();
            dbConn = DriverManager.getConnection(dbURL, dbUser, dbPassWord);
            return dbConn;
        } 
        catch (ClassNotFoundException e) {
        } 
        catch (SQLException e) {
            e.printStackTrace();
        } 
        catch (InstantiationException e) {
        } 
        catch (IllegalAccessException e) {
        }  
        return dbConn;
    }
    public ArrayList DbSelect(String sqlstatement, Connection dbConn) {
        ArrayList<Map> resultList = new ArrayList<Map>();
        try {
	    Statement stmt = dbConn.createStatement();
	    ResultSet resultset = stmt.executeQuery(sqlstatement);
            ResultSetMetaData rsmd = resultset.getMetaData();
            int columns = rsmd.getColumnCount();
            int i = 0;
            while (resultset.next()) {
                int col = 1;
                Map resultMap = new HashMap();
                while (col <= columns) {
                    String value = resultset.getString(col);
                    String colName = rsmd.getColumnName(col);
                    resultMap.put(colName, value);
                    col++;
                }
                resultList.add(i, resultMap);
                i++;
            }
            resultset.close();
            stmt.close();
	} catch(SQLException ex) {
            ex.printStackTrace();
	}
        return(resultList);
    }
    public void DBstatement(String sqlstatement, Connection dbConn) {
        try {
            Statement stmt = dbConn.createStatement();
            stmt.executeUpdate(sqlstatement);
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(OraDB.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
}
