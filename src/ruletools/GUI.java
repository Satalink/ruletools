/*
 * GUI.java
 *
 * Created on July 30, 2008, 3:01 PM
 */

package ruletools;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author  ag991n
 */
public class GUI extends javax.swing.JFrame {
    public Map<String, Color> colorMap;
    public Map<String, Font> fontMap;
    public Map<String, Integer> typeMap;
    public Map<Integer, String> typeNameMap;
    public Map<String, DefaultMutableTreeNode> treeMap;
    public Map<String, ImageIcon> extMap;
    public Map<String, JMenuItem> menuMap;
    public ArrayList databases = new ArrayList();
    public JFileChooser fc = new JFileChooser();
    public StyledDocument view;
    private Style[] styles = new Style[24];;
    private Map<String, String> searchMap;
    private Pkg_Interface iface = null;
    private TableSorter sorter;

    /** Creates new form GUI */
    public GUI(Pkg_Interface aface) {
        iface = aface;
        searchMap = new HashMap<String, String>();
        searchMap.put("EventTable", "select class.name as \"ClassName\", mess.name as \"Name\", eventid.ordr + 1 as \"LineNumber\" from eventid join mess on mess.mess = eventid.eventid join class on class.CLASS = mess.class ");
        searchMap.put("GatewayTable", "select class.name as \"ClassName\", mess.name as \"Name\", messagerule.ordr + 1 as \"LineNumber\" from messagerule join mess on mess.mess = messagerule.mess join class on class.class = mess.class ");
        searchMap.put("MethodTable", "select class.name as \"ClassName\", rulemethod.name as \"Name\", methodbody.ordr + 1 as \"LineNumber\" from methodbody join rulemethod on rulemethod.methodid = methodbody.methodid join class on class.class = rulemethod.xclass ");
        searchMap.put("DialogTable", "select class.name as \"ClassName\", dialog.dialogname as \"Name\", dialogcmd.ordr as \"LineNumber\" from dialogcmd join dialog on dialog.startstate = dialogcmd.state join class on class.class = dialog.class ");
        searchMap.put("EventCode", "select class.name as \"ClassName\", mess.name as \"Name\", eventid.ordr + 1 as \"LineNumber\", eventid.operator as \"Operator\", eventid.operand1 as \"Operand1\", eventid.operand2 as \"Operand2\", eventid.relate as \"Relate\", eventid.event as \"EventID\", eventid.alertid as \"AlertID\" from eventid join mess on mess.mess = eventid.eventid join class on class.CLASS = mess.class where mess.name = '");
        searchMap.put("GatewayCode", "select class.name as \"ClassName\", mess.name as \"Name\", messagerule.ordr + 1 as \"LineNumber\", messagerule.operator as \"Operator\", messagerule.operand1 as \"Operand1\", messagerule.operand2 as \"Operand2\", messagerule.relate as \"Relate\", messagerule.event as \"EventID\", messagerule.encoded as \"AlertID\" from messagerule join mess on mess.mess = messagerule.mess join class on class.CLASS = mess.class where mess.name = '");
        searchMap.put("MethodCode", "select class.name as \"ClassName\", rulemethod.name as \"Name\", methodbody.ordr + 1 as \"LineNumber\", methodbody.operator as \"Operator\", methodbody.operand1 as \"Operand1\", methodbody.operand2 as \"Operand2\", methodbody.relate as \"Relate\", methodbody.event as \"EventID\", methodbody.alertid as \"AlertID\" from methodbody join rulemethod on rulemethod.methodid = methodbody.methodid join class on class.class = rulemethod.xclass where rulemethod.name = '");
        searchMap.put("DialogCode", "select class.name as \"ClassName\", dialog.dialogname as \"Name\", dialogcmd.ordr as \"LineNumber\", dialogcmd.operator as \"Operator\", dialogcmd.operand1 as \"Operand1\", dialogcmd.operand2 as \"Operand2\", dialogcmd.relate as \"Relate\", dialogcmd.event as \"EventID\", dialogcmd.encoded as \"AlertID\" from dialogcmd join dialog on dialog.startstate = dialogcmd.state join class on class.class = dialog.class where dialog.dialogname = '");
        searchMap.put("EventTableOrderBy", "ORDER BY class.name, mess.name, eventid.ordr");
        searchMap.put("GatewayTableOrderBy", "ORDER BY class.name, mess.name, messagerule.ordr");
        searchMap.put("MethodTableOrderBy", "ORDER BY class.name, rulemethod.name, methodbody.ordr");
        searchMap.put("DialogTableOrderBy", "ORDER BY class.name, dialog.dialogname, dialogcmd.ordr");
        searchMap.put("EventCodeOrderBy", "ORDER BY eventid.ordr");
        searchMap.put("GatewayCodeOrderBy", "ORDER BY messagerule.ordr");
        searchMap.put("MethodCodeOrderBy", "ORDER BY methodbody.ordr");
        searchMap.put("DialogCodeOrderBy", "ORDER BY dialogcmd.ordr");
        searchMap.put("EventName", "select class.name as \"ClassName\", mess.name as \"Name\" from mess join class on class.class = mess.class where mess.mess = ");
        searchMap.put("GatewayName", "select class.name as \"ClassName\", mess.name as \"Name\" from mess join class on class.class = mess.class where mess.mess = ");
        searchMap.put("MethodName", "select class.name as \"ClassName\", rulemethod.name as \"Name\" from rulemethod join class on class.class = rulemethod.xclass where rulemethod.methodid = ");
        searchMap.put("DialogName", "select class.name as \"ClassName\", dialog.dialogname as \"Name\" from dialog join class on class.class = dialog.class where dialog.dialog = ");
        searchMap.put("EventHeader", "select mess.parent as \"Parent\", mess.priority as \"Priority\", mess.description as \"Description\" from mess join class on class.class = mess.class where mess.name = \'");
        searchMap.put("GatewayHeader", "select mess.parent as \"Parent\", mess.priority as \"Priority\", mess.description as \"Description\" from mess join class on class.class = mess.class where mess.name = \'");
        searchMap.put("MethodHeader", "select rulemethod.description as \"Description\", rulemethod.whereused as \"WHEREUSED\" from rulemethod join class on class.class = rulemethod.xclass where rulemethod.name = \'");
        searchMap.put("EventCheck", "select max(eventid.ordr) as \"LineCount\" from eventid join mess on mess.mess = eventid.eventid join class on class.class = mess.class where mess.name = '");
        searchMap.put("GatewayCheck", "select max(messagerule.ordr) as \"LineCount\" from messagerule join mess on mess.mess = messagerule.mess join class on class.class = mess.class where mess.name = '");
        searchMap.put("FormalParms", "select formalparms.ordr +1 as \"ORDER\", attrbute.name as \"ATTRIBUTE\", formalparms.passby as \"PASSBY\" from FORMALPARMS join attrbute on formalparms.attribute = attrbute.attrbute where formalparms.methodid in (select rulemethod.methodid from rulemethod join class on rulemethod.xclass = class.class ");
        
        colorMap = new HashMap<String, Color>();
            colorMap.put("black", new Color(0,0,0));
            colorMap.put("white", new Color(255,255,255));
            colorMap.put("header", new Color(200,200,200));
            colorMap.put("menu", new Color(220,220,220));
            colorMap.put("skyblue", new Color(135,206,235));
            colorMap.put("cloudwhite", new Color(209,238,238));
        fontMap = new HashMap<String, Font>();
            fontMap.put("GA10", new Font("Georgia", 0, 10));
            fontMap.put("GA10B", new Font("Georgia", 1, 10));
            fontMap.put("GA11", new Font("Georgia", 0, 11));
            fontMap.put("GA11B", new Font("Georgia", 1, 11));
            fontMap.put("GA12B", new Font("Georgia", 1, 12));
            fontMap.put("GA14", new Font("Georgia", 0, 14));
            fontMap.put("GA14B", new Font("Georgia", 1, 14));
            fontMap.put("GA16", new Font("Georgia", 0, 16));
            fontMap.put("GA16B", new Font("Georgia", 1, 16));	
        typeNameMap = new HashMap<Integer, String>();
            typeNameMap.put(0, "Event");
            typeNameMap.put(1, "Gateway");
            typeNameMap.put(2, "Method");
            typeNameMap.put(3, "Dialog");
        typeMap = new HashMap<String, Integer>();
        for(int i=0;i < typeNameMap.size();i++) {
            typeMap.put(typeNameMap.get(i).toString(), i);
        }    
        extMap = new HashMap<String, ImageIcon>();
            //NetExpert Extensions
            extMap.put("evt", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("mth", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("dlg", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("alt", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("att", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("abs", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("cls", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("mos", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("pol", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("pls", new ImageIcon(iface.getURL("NetExpert.png")));
            extMap.put("rel", new ImageIcon(iface.getURL("NetExpert.png")));
            // Script Extensions
            extMap.put("csh", new ImageIcon(iface.getURL("Shell.png")));
            extMap.put("ksh", new ImageIcon(iface.getURL("Shell.png")));
            extMap.put("sh", new ImageIcon(iface.getURL("Shell.png")));
            extMap.put("pl", new ImageIcon(iface.getURL("Perl.png")));
            extMap.put("java", new ImageIcon(iface.getURL("Java.png")));
            extMap.put("jar", new ImageIcon(iface.getURL("Java.png")));
            
        treeMap = new HashMap<String, DefaultMutableTreeNode>();
        initComponents();
        setSplitPaneY(250);
        initTreeSVN();
        jTable_Results.getTableHeader().setDefaultRenderer(new CustomHeaderRenderer());
        sorter = new TableSorter(jTable_Results.getModel());
        jTable_Results.setModel(sorter);
        sorter.setTableHeader(jTable_Results.getTableHeader());
        jTable_Results.setTableHeader(sorter.getTableHeader());
        int[] colsizes = new int[]{180,180,56,0};
           for (int i=0; i < jTable_Results.getModel().getColumnCount();i++) {
               jTable_Results.getColumnModel().getColumn(i).setCellRenderer(new CustomCellRenderer());
               jTable_Results.getColumnModel().getColumn(i).setPreferredWidth(colsizes[i]);
           }
        //Window Listener
	addWindowListener(new WindowAdapter() {
	    @Override
	public void windowClosing(WindowEvent ex) {
            iface.exitApp();
	}
	});        
        //jTreeSVN MouseListener
        MouseListener jTreeSVNMouseListener = new MouseAdapter() {
	    @Override
            public void mousePressed(MouseEvent mc) {
                int selRow = jTreeSVN.getRowForLocation(mc.getX(), mc.getY());
                TreePath selPath = jTreeSVN.getPathForLocation(mc.getX(), mc.getY());
                if(selRow != -1) {
                    if(mc.getButton() == MouseEvent.BUTTON1) {
                        if(mc.getClickCount() == 1) {
                             jTreeSVNSingleClick(selPath);
    //                    } else if(mc.getClickCount() == 2) {
    //                        iface.setCursor("Wait");
    //                        jTreeSVNDoubleClick(selPath, false);
                        }
                    }
                }
            }
        };
        jTreeSVN.addMouseListener(jTreeSVNMouseListener);
        // jList_Events MoustListener
        MouseListener jList_EventsMouseListener = new MouseAdapter() {
	    @Override
            public void mousePressed(MouseEvent mc) {
                String type = "Event";
                JList list = (JList) jList_Events.getComponentAt(mc.getX(), mc.getY());
                String name = (String) list.getSelectedValue();
                if(name != null) {
                    if(mc.getButton() == MouseEvent.BUTTON1) {
                        if(mc.getClickCount() == 1) {
                            try {
                                view = (StyledDocument) jTextPane_Reader.getDocument();
                                view = setStyles(view);
                                view = clearViewer(view);
                                String classname = jComboBox_Class.getSelectedItem().toString();
                                view = createEventHeader(view, typeMap.get(type), type, classname, name);                                
                                String sql = searchMap.get("GatewayCheck")+name+"' and class.name = '"+classname+"'";
                                ArrayList data = iface.getDBquery(sql);
                                Map dataMap = (Map) data.get(0);
                                if(dataMap.get("LineCount") != null) {
                                    view.insertString(view.getLength(), "\tGateway Analysis:\n\n", styles[0]);
                                    jListItem_ActionPerformed("Gateway", name);
                                }
                                sql = searchMap.get("EventCheck")+name+"' and class.name = '"+classname+"'";
                                data = iface.getDBquery(sql);
                                dataMap = (Map) data.get(0);
                                if(dataMap.get("LineCount") != null) {                                
                                    view.insertString(view.getLength(), "\tEvent Analysis:\n\n", styles[0]);
                                    jListItem_ActionPerformed(type, name);
                                }
                            } catch (BadLocationException ex) {
                                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        };
        jList_Events.addMouseListener(jList_EventsMouseListener);
        // jList_Methods MoustListener
        MouseListener jList_MethodsMouseListener = new MouseAdapter() {
	    @Override
            public void mousePressed(MouseEvent mc) {
                String type = "Method";
                JList list = (JList) jList_Methods.getComponentAt(mc.getX(), mc.getY());
                String name = (String) list.getSelectedValue();
                if(name != null) {
                    if(mc.getButton() == MouseEvent.BUTTON1) {
                        if(mc.getClickCount() == 1) {
                            try {
                                view = (StyledDocument) jTextPane_Reader.getDocument();
                                view = setStyles(view);
                                view = clearViewer(view);
                                String classname = jComboBox_Class.getSelectedItem().toString();
                                view = createEventHeader(view, typeMap.get(type), type, classname, name);
                                view.insertString(view.getLength(), "\tRule Method:\n\n", styles[0]);
                                jListItem_ActionPerformed(type, name);
                            } catch (BadLocationException ex) {
                                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        };
        jList_Methods.addMouseListener(jList_MethodsMouseListener);        
        // jList_Dialogs MoustListener
        MouseListener jList_DialogsMouseListener = new MouseAdapter() {
	    @Override
            public void mousePressed(MouseEvent mc) {
                String type = "Dialog";
                JList list = (JList) jList_Dialogs.getComponentAt(mc.getX(), mc.getY());
                String name = (String) list.getSelectedValue();
                if(name != null) {
                    if(mc.getButton() == MouseEvent.BUTTON1) {
                        if(mc.getClickCount() == 1) {
                            try {
                                view = (StyledDocument) jTextPane_Reader.getDocument();
                                view = setStyles(view);
                                view = clearViewer(view);
                                String classname = jComboBox_Class.getSelectedItem().toString();
                                view = createEventHeader(view, typeMap.get(type), type, classname, name);
                                jListItem_ActionPerformed(type, name);
                            } catch (BadLocationException ex) {
                                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        };
        jList_Dialogs.addMouseListener(jList_DialogsMouseListener);
        createTextPaneReaderPopupMenu();
        createTextPaneViewerPopupMenu();
    }       
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane_Main = new javax.swing.JTabbedPane();
        ReadPanel = new javax.swing.JPanel();
        jSplitPane_Reader = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox_Class = new javax.swing.JComboBox();
        jTabbedPane_Reader = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList_Events = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList_Methods = new javax.swing.JList();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jList_Dialogs = new javax.swing.JList();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextPane_Reader = new javax.swing.JTextPane();
        SearchPanel = new javax.swing.JPanel();
        jRadioButton_Events = new javax.swing.JRadioButton();
        jRadioButton_Methods = new javax.swing.JRadioButton();
        jRadioButton_Dialogs = new javax.swing.JRadioButton();
        jComboBox_Fields = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jComboBox_Operator = new javax.swing.JComboBox();
        jTextField_SearchString = new javax.swing.JTextField();
        jButton_Go = new javax.swing.JButton();
        jSplitPane_Search = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane_Viewer = new javax.swing.JTextPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_Results = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jSplitPane_SVN = new javax.swing.JSplitPane();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTreeDIR = new javax.swing.JTree();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTreeSVN = new javax.swing.JTree();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu_File = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu_Databases = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("Class: ");

        jComboBox_Class.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox_Class.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_ClassActionPerformed(evt);
            }
        });

        jList_Events.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jList_Events);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );

        jTabbedPane_Reader.addTab("Events", jPanel2);

        jList_Methods.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList_Methods);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );

        jTabbedPane_Reader.addTab("Methods", jPanel3);

        jList_Dialogs.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane5.setViewportView(jList_Dialogs);

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );

        jTabbedPane_Reader.addTab("Dialogs", jPanel4);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jComboBox_Class, 0, 169, Short.MAX_VALUE))
            .add(jTabbedPane_Reader, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBox_Class, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTabbedPane_Reader, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE))
        );

        jSplitPane_Reader.setLeftComponent(jPanel1);

        jScrollPane6.setViewportView(jTextPane_Reader);

        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE)
        );

        jSplitPane_Reader.setRightComponent(jPanel8);

        org.jdesktop.layout.GroupLayout ReadPanelLayout = new org.jdesktop.layout.GroupLayout(ReadPanel);
        ReadPanel.setLayout(ReadPanelLayout);
        ReadPanelLayout.setHorizontalGroup(
            ReadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSplitPane_Reader)
        );
        ReadPanelLayout.setVerticalGroup(
            ReadPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSplitPane_Reader, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 708, Short.MAX_VALUE)
        );

        jTabbedPane_Main.addTab("Rule Reader", ReadPanel);

        SearchPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroup1.add(jRadioButton_Events);
        jRadioButton_Events.setText("Events"); // NOI18N

        buttonGroup1.add(jRadioButton_Methods);
        jRadioButton_Methods.setText("Methods"); // NOI18N

        buttonGroup1.add(jRadioButton_Dialogs);
        jRadioButton_Dialogs.setText("Dialogs"); // NOI18N

        jComboBox_Fields.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Operator", "Operand1", "Operand2", "Relate", "EventName", "AlertName" }));

        jLabel1.setText("Field"); // NOI18N

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jComboBox_Operator.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "is", "has" }));

        jTextField_SearchString.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_SearchStringActionPerformed(evt);
            }
        });

        jButton_Go.setText("Go"); // NOI18N
        jButton_Go.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_GoActionPerformed(evt);
            }
        });

        jSplitPane_Search.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jScrollPane2.setToolTipText("Value = "+jScrollPane2.getVerticalScrollBar().getValue()+"");

        jTextPane_Viewer.setEditable(false);
        jScrollPane2.setViewportView(jTextPane_Viewer);

        jSplitPane_Search.setRightComponent(jScrollPane2);

        jTable_Results.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Class", "Name", "Line #", "Type"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable_Results);
        jTable_Results.getColumnModel().getColumn(3).setResizable(false);

        jSplitPane_Search.setLeftComponent(jScrollPane1);

        org.jdesktop.layout.GroupLayout SearchPanelLayout = new org.jdesktop.layout.GroupLayout(SearchPanel);
        SearchPanel.setLayout(SearchPanelLayout);
        SearchPanelLayout.setHorizontalGroup(
            SearchPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(SearchPanelLayout.createSequentialGroup()
                .add(jRadioButton_Events)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jRadioButton_Methods)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jRadioButton_Dialogs)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jComboBox_Fields, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jComboBox_Operator, 0, 120, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTextField_SearchString, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 354, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButton_Go))
            .add(jSplitPane_Search, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 888, Short.MAX_VALUE)
        );
        SearchPanelLayout.setVerticalGroup(
            SearchPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(SearchPanelLayout.createSequentialGroup()
                .add(SearchPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(SearchPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jRadioButton_Events)
                        .add(jRadioButton_Methods)
                        .add(jRadioButton_Dialogs))
                    .add(SearchPanelLayout.createSequentialGroup()
                        .add(11, 11, 11)
                        .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(SearchPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jComboBox_Fields, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jButton_Go)
                        .add(jTextField_SearchString, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jComboBox_Operator, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSplitPane_Search, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 675, Short.MAX_VALUE))
        );

        jTabbedPane_Main.addTab("Rule Search", SearchPanel);

        jScrollPane8.setViewportView(jTreeDIR);

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE)
        );

        jSplitPane_SVN.setLeftComponent(jPanel6);

        jScrollPane7.setViewportView(jTreeSVN);

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 862, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE)
        );

        jSplitPane_SVN.setRightComponent(jPanel7);

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSplitPane_SVN, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 892, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSplitPane_SVN, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 708, Short.MAX_VALUE)
        );

        jTabbedPane_Main.addTab("SVN Tool", jPanel5);

        jMenu_File.setText("File"); // NOI18N
        jMenu_File.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu_FileActionPerformed(evt);
            }
        });

        jMenuItem2.setText("Properties");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu_File.add(jMenuItem2);

        jMenuItem1.setText("Exit"); // NOI18N
        jMenu_File.add(jMenuItem1);

        jMenuBar1.add(jMenu_File);

        jMenu_Databases.setText("Databases");
        jMenuBar1.add(jMenu_Databases);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTabbedPane_Main)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTabbedPane_Main, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void jTextField_SearchStringActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_SearchStringActionPerformed
        jButton_GoActionPerformed(evt);
}//GEN-LAST:event_jTextField_SearchStringActionPerformed
    private void jButton_GoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_GoActionPerformed
        String[] types = {"Event", "Gateway", "Method", "Dialog"};
        if(!jTextField_SearchString.equals(null)) {
            if(jRadioButton_Events.isSelected()) {
                updateTable(iface.getDBquery(getSqlString(types[0])), types[0]);                
                updateTable(iface.getDBquery(getSqlString(types[1])), types[1]);
            } else 
            if(jRadioButton_Methods.isSelected()) {
                updateTable(iface.getDBquery(getSqlString(types[2])), types[2]);
            } else 
            if(jRadioButton_Dialogs.isSelected()) {
                updateTable(iface.getDBquery(getSqlString(types[3])), types[3]);
            } else {
                for(int s=0;s < (buttonGroup1.getButtonCount() + 1);s++) {
                    updateTable(iface.getDBquery(getSqlString(types[s])), types[s]);
                }
            }
        }
    }//GEN-LAST:event_jButton_GoActionPerformed
    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        iface.exitApp();
    }//GEN-LAST:event_jMenu1ActionPerformed
    private void jComboBox_ClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_ClassActionPerformed
        jList_Events.removeAll();
        jList_Methods.removeAll();
        jList_Dialogs.removeAll();
        String sql;
        ArrayList eventList;
        Object[] listObj;
        if(jComboBox_Class.getSelectedItem() == null) return;
        sql = "select mess.name as \"Name\" from mess join class on class.class = mess.class where class.name = '"+jComboBox_Class.getSelectedItem().toString()+"' order by mess.name";
        jList_Events.setModel(setListModel(iface.getDBquery(sql), "Events"));
        sql = "select rulemethod.name as \"Name\" from rulemethod join class on class.class = rulemethod.xclass where class.name = '"+jComboBox_Class.getSelectedItem().toString()+"' order by rulemethod.name";
        jList_Methods.setModel(setListModel(iface.getDBquery(sql), "Methods"));
        sql = "select dialog.dialogname as \"Name\" from dialog join class on class.class = dialog.class where class.name = '"+jComboBox_Class.getSelectedItem().toString()+"' order by dialog.dialogname";
        jList_Dialogs.setModel(setListModel(iface.getDBquery(sql), "Dialogs"));        
    }//GEN-LAST:event_jComboBox_ClassActionPerformed
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        GUI_Options options = new GUI_Options(iface);
        int x = getLocation().x + (getSize().width/3);
        int y = getLocation().y + (getSize().height/5);
        options.setLocation(x, y);
        options.setVisible(true);    
}//GEN-LAST:event_jMenuItem2ActionPerformed
    private void jMenu_FileActionPerformed(java.awt.event.ActionEvent evt) {
        
    }
    private void jListItem_ActionPerformed(String type, String selection) throws BadLocationException {
        createDocumentFromList(type, selection);
    }
    private void jButton_evtButtonActionPerformed(java.awt.event.ActionEvent evt, int eventid) throws BadLocationException {
        createDocumentFromButtonAction(clearViewer(view), eventid, "Event");
    }
    private void jButton_mthButtonActionPerformed(java.awt.event.ActionEvent evt, int methodid) throws BadLocationException {
        createDocumentFromButtonAction(clearViewer(view), methodid, "Method");
    }   
    private void jButton_dlgButtonActionPerformed(java.awt.event.ActionEvent evt, int dialogid) throws BadLocationException {
        createDocumentFromButtonAction(clearViewer(view), dialogid, "Dialog");
    }    
    private void jTextPane_ActionPerformed(java.awt.event.ActionEvent evt) {
        if(evt.getActionCommand().equals("Print")) {
            printDocument(jTextPane_Reader);
        } else
        if(evt.getActionCommand().equals("Copy")) {
            copyDocument(jTextPane_Reader);
        }
    }
  
    private void jTreeSVNSingleClick(TreePath selPath) {

    }
    
    protected void clearTable() {
        DefaultTableModel model = (DefaultTableModel) sorter.getTableModel();
        for (int i=(model.getRowCount() - 1);i >= 0;i--) {
            model.removeRow(i);
        }
    }    
    protected StyledDocument clearViewer(StyledDocument doc) throws BadLocationException {
        doc.remove(0, doc.getLength());
        return(doc);
    }
    
    public void createjTreeNode(JTree tree,DefaultMutableTreeNode node,String text) {
        CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
        tree.setCellRenderer(custom);
        Vector contents = getSVNlist(text);
        for (int i=0;i < contents.size();i++) {
            DefaultMutableTreeNode newnode = new DefaultMutableTreeNode(contents.get(i).toString());
            node.add(newnode);
        }
    }   
    public void createjTreeNodeFiles(JTree tree,DefaultMutableTreeNode node,File file) {
        if(file.isDirectory()) {
            CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
            tree.setCellRenderer(custom);
            File[] files = file.listFiles();
            for (int i=0;i < files.length;i++) {
                if(files[i].isDirectory()) { 
                            DefaultMutableTreeNode newnode = new DefaultMutableTreeNode(files[i].getName());
                    node.add(newnode);
                    createjTreeNodeFiles(tree, newnode, files[i]);
                }
            }
        }
    }    
    
    protected Map getColorMap() {
        return(colorMap);
    }
    protected Map getFontMap() {
        return(fontMap);
    }
    protected String getSqlString(String type) {
        String sql = null;
        //Data PreProcess
        if(jComboBox_Operator.getSelectedItem().toString().equals("is")) {
            if(jComboBox_Fields.getSelectedItem().toString().equals("EventName")) {
                sql = searchMap.get(type+"Table").toString()+" WHERE event in (select eventid.event from eventid join mess on mess.mess = eventid.event where mess.name = \'"+jTextField_SearchString.getText().toString()+"\') "+searchMap.get(type+"TableOrderBy").toString();
            } else
            if(jComboBox_Fields.getSelectedItem().toString().equals("AlertName")) {
                sql = searchMap.get(type+"Table").toString()+" WHERE alertid in (select eventid.alertid from eventid join alertid on alertid.alertid = eventid.alertid where alertid.name = \'"+jTextField_SearchString.getText().toString()+"\') "+searchMap.get(type+"TableOrderBy").toString();
            } else {
                sql = searchMap.get(type+"Table").toString()+" WHERE "+jComboBox_Fields.getSelectedItem().toString()+" = \'"+jTextField_SearchString.getText().toString()+"\' "+searchMap.get(type+"TableOrderBy").toString();
            }
        } else
        if(jComboBox_Operator.getSelectedItem().toString().equals("has")) {
            sql = searchMap.get(type+"Table").toString()+" WHERE "+jComboBox_Fields.getSelectedItem().toString()+" like \'%"+jTextField_SearchString.getText().toString()+"%\' "+searchMap.get(type+"TableOrderBy").toString(); 
        }
        return(sql);                
    }
    protected Vector getSVNlist(String path) {
        Vector contents = new Vector();
        try {
            String line = null;
            Process svnls = Runtime.getRuntime().exec("svn ls " + path.toString() + "");
            BufferedReader svnin = new BufferedReader(new InputStreamReader(svnls.getInputStream()));
            while((line = svnin.readLine()) != null) {
                contents.add(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return(contents);
    }
    public Icon getSystemFileIcon(File file) {
        Icon icon = null;
        if(file.exists()) {
            icon = fc.getFileSystemView().getSystemIcon(file);	
        }
        return(icon);
    }
    public Properties getGUIprops(Properties properties) {
        properties.setProperty("GUI.Reader.jSplitPane_Reader", String.valueOf(jSplitPane_Reader.getDividerLocation()));
        properties.setProperty("GUI.Reader.jComboBox_Class", jComboBox_Class.getSelectedItem().toString());
        properties.setProperty("GUI.Search.jSplitPane_Search", String.valueOf(jSplitPane_Search.getDividerLocation()));
        properties.setProperty("GUI.SVN.jSplitPane_SVN", String.valueOf(jSplitPane_SVN.getDividerLocation()));     
        return(properties);
    }
    private void initTreeSVN() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(iface.getSVNroot());
        jTreeSVN.setLargeModel(true);
        jTreeSVN.setBackground(colorMap.get("menu"));
        DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
        jTreeSVN.setModel(treeModel);
        jTreeSVN.setFont(fontMap.get("GA10B"));
        CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
        jTreeSVN.setCellRenderer(custom);
        createjTreeNode(jTreeSVN, rootNode, iface.getSVNroot());
        jTreeSVN.updateUI();
        treeMap.put(iface.getSVNroot(), (DefaultMutableTreeNode)rootNode);
        ToolTipManager.sharedInstance().registerComponent(jTreeSVN);
        ToolTipManager.sharedInstance().setInitialDelay(1500);
        ToolTipManager.sharedInstance().setDismissDelay(30000);
        ToolTipManager.sharedInstance().setReshowDelay(15000);
    }
    public void initReader(Properties props) {
        if(iface.getConnStat()) {
            jComboBox_Class.removeAllItems();
            String sql = "select class.name as \"Name\" from class where class in ( select class from mess ) or class in ( select xclass from rulemethod ) or class in ( select class from dialog ) order by name";
            ArrayList classList = iface.getDBquery(sql);
            Object[] classObj = new Object[classList.size()];
            for(int cls=0;cls < classList.size();cls++) {
                Map classMap = (Map) classList.get(cls);
                classObj[cls] = classMap.get("Name").toString();
                jComboBox_Class.addItem(classObj[cls]);
            }        
            setGUIprops(props);
        }
    }
    public void setGUIprops(Properties properties) {
        jSplitPane_Reader.setDividerLocation(Integer.valueOf(properties.getProperty("GUI.Reader.jSplitPane_Reader")));
        jSplitPane_Search.setDividerLocation(Integer.valueOf(properties.getProperty("GUI.Search.jSplitPane_Search")));
        jSplitPane_SVN.setDividerLocation(Integer.valueOf(properties.getProperty("GUI.SVN.jSplitPane_SVN")));
        jComboBox_Class.setSelectedItem(properties.getProperty("GUI.Reader.jComboBox_Class"));
    }
    private DefaultListModel setListModel(ArrayList eventlist, String ruletype) {
        DefaultListModel listmodel = new DefaultListModel();
        for(int i=0;i < eventlist.size();i++) {
            Map listMap = (Map) eventlist.get(i);
            listmodel.add(i, listMap.get("Name").toString());
        }
        setTabLabels(ruletype, listmodel.size());
        return(listmodel);
    }
    public void setSplitPaneY(int splity) {
        jSplitPane_Search.setDividerLocation(splity);
    }
    public StyledDocument setStyles(StyledDocument doc) {
        //header
        Style styleH1 = doc.addStyle("header", null);
        StyleConstants.setItalic(styleH1, false);
        StyleConstants.setBold(styleH1, true);
        StyleConstants.setFontFamily(styleH1, "Georgia");
        StyleConstants.setFontSize(styleH1, 18);
        StyleConstants.setBackground(styleH1, Color.WHITE);
        StyleConstants.setForeground(styleH1, Color.BLUE);
        styles[0] = styleH1;
        //Normal
        Style styleN = doc.addStyle("normal", null);
        StyleConstants.setItalic(styleN, false);
        StyleConstants.setBold(styleN, false);
        StyleConstants.setFontFamily(styleN, "Georgia");
        StyleConstants.setFontSize(styleN, 12);
        StyleConstants.setBackground(styleN, Color.WHITE);
        StyleConstants.setForeground(styleN, Color.BLACK);
        styles[1] = styleN;
        //ITALIC
        Style styleNI = doc.addStyle("normalItalic", null);
        StyleConstants.setItalic(styleNI, true);
        StyleConstants.setBold(styleNI, false);
        StyleConstants.setFontFamily(styleNI, "Georgia");
        StyleConstants.setFontSize(styleNI, 12);
        StyleConstants.setBackground(styleNI, Color.WHITE);
        StyleConstants.setForeground(styleNI, Color.BLACK);
        styles[2] = styleNI;
        //BOLD
        Style styleNB = doc.addStyle("normalBold", null);
        StyleConstants.setItalic(styleNB, false);
        StyleConstants.setBold(styleNB, true);
        StyleConstants.setFontFamily(styleNB, "Georgia");
        StyleConstants.setFontSize(styleNB, 12);
        StyleConstants.setBackground(styleNB, Color.WHITE);
        StyleConstants.setForeground(styleNB, Color.BLACK);
        styles[3] = styleNB;
        //BOLD ITALIC
        Style styleNBI = doc.addStyle("normalBoldItalic", null);
        StyleConstants.setItalic(styleNBI, true);
        StyleConstants.setBold(styleNBI, true);
        StyleConstants.setFontFamily(styleNBI, "Georgia");
        StyleConstants.setFontSize(styleNBI, 12);
        StyleConstants.setBackground(styleNBI, Color.WHITE);
        StyleConstants.setForeground(styleNBI, Color.BLACK);
        styles[4] = styleNBI;
        Style styleNblue = doc.addStyle("normalBlue", null);
        StyleConstants.setItalic(styleNblue, false);
        StyleConstants.setBold(styleNblue, false);
        StyleConstants.setFontFamily(styleNblue, "Georgia");
        StyleConstants.setFontSize(styleNblue, 12);
        StyleConstants.setBackground(styleNblue, Color.WHITE);
        StyleConstants.setForeground(styleNblue, Color.BLUE);
        styles[5] = styleNblue;
        Style styleNblueI = doc.addStyle("normalBlueItalic", null);
        StyleConstants.setItalic(styleNblueI, true);
        StyleConstants.setBold(styleNblueI, false);
        StyleConstants.setFontFamily(styleNblueI, "Georgia");
        StyleConstants.setFontSize(styleNblueI, 12);
        StyleConstants.setBackground(styleNblueI, Color.WHITE);
        StyleConstants.setForeground(styleNblueI, Color.BLUE);
        styles[6] = styleNblueI;
        Style styleNblueB = doc.addStyle("normalBlueBold", null);
        StyleConstants.setItalic(styleNblueB, false);
        StyleConstants.setBold(styleNblueB, true);
        StyleConstants.setFontFamily(styleNblueB, "Georgia");
        StyleConstants.setFontSize(styleNblueB, 12);
        StyleConstants.setBackground(styleNblueB, Color.WHITE);
        StyleConstants.setForeground(styleNblueB, Color.BLUE);
        styles[7] = styleNblueB; 
        Style styleNblueBI = doc.addStyle("normalBlueBoldItalic", null);
        StyleConstants.setItalic(styleNblueBI, true);
        StyleConstants.setBold(styleNblueBI, true);
        StyleConstants.setFontFamily(styleNblueBI, "Georgia");
        StyleConstants.setFontSize(styleNblueBI, 12);
        StyleConstants.setBackground(styleNblueBI, Color.WHITE);
        StyleConstants.setForeground(styleNblueBI, Color.BLUE);
        styles[8] = styleNblueBI;        
        Style styleNred = doc.addStyle("normalRed", null);
        StyleConstants.setItalic(styleNred, false);
        StyleConstants.setBold(styleNred, false);
        StyleConstants.setFontFamily(styleNred, "Georgia");
        StyleConstants.setFontSize(styleNred, 12);
        StyleConstants.setBackground(styleNred, Color.WHITE);
        StyleConstants.setForeground(styleNred, Color.RED);
        styles[9] = styleNred;
        Style styleNredI = doc.addStyle("normalRedItalic", null);
        StyleConstants.setItalic(styleNredI, true);
        StyleConstants.setBold(styleNredI, false);
        StyleConstants.setFontFamily(styleNredI, "Georgia");
        StyleConstants.setFontSize(styleNredI, 12);
        StyleConstants.setBackground(styleNredI, Color.WHITE);
        StyleConstants.setForeground(styleNredI, Color.RED);
        styles[10] = styleNredI;
        Style styleNredB = doc.addStyle("normalRedBold", null);
        StyleConstants.setItalic(styleNredB, false);
        StyleConstants.setBold(styleNredB, true);
        StyleConstants.setFontFamily(styleNredB, "Georgia");
        StyleConstants.setFontSize(styleNredB, 12);
        StyleConstants.setBackground(styleNredB, Color.WHITE);
        StyleConstants.setForeground(styleNredB, Color.RED);
        styles[11] = styleNredB; 
        Style styleNredBI = doc.addStyle("normalRedBoldItalic", null);
        StyleConstants.setItalic(styleNredBI, true);
        StyleConstants.setBold(styleNredBI, true);
        StyleConstants.setFontFamily(styleNredBI, "Georgia");
        StyleConstants.setFontSize(styleNredBI, 12);
        StyleConstants.setBackground(styleNredBI, Color.WHITE);
        StyleConstants.setForeground(styleNredBI, Color.RED);
        styles[12] = styleNredBI;        
        Style styleNgreen = doc.addStyle("normalgreen", null);
        StyleConstants.setItalic(styleNgreen, false);
        StyleConstants.setBold(styleNgreen, false);
        StyleConstants.setFontFamily(styleNgreen, "Georgia");
        StyleConstants.setFontSize(styleNgreen, 12);
        StyleConstants.setBackground(styleNgreen, Color.WHITE);
        StyleConstants.setForeground(styleNgreen, Color.GREEN.darker());
        styles[13] = styleNgreen;
        Style styleNgreenI = doc.addStyle("normalGreenItalic", null);
        StyleConstants.setItalic(styleNgreenI, true);
        StyleConstants.setBold(styleNgreenI, false);
        StyleConstants.setFontFamily(styleNgreenI, "Georgia");
        StyleConstants.setFontSize(styleNgreenI, 12);
        StyleConstants.setBackground(styleNgreenI, Color.WHITE);
        StyleConstants.setForeground(styleNgreenI, Color.GREEN.darker());
        styles[14] = styleNgreenI;
        Style styleNgreenB = doc.addStyle("normalGreenBold", null);
        StyleConstants.setItalic(styleNgreenB, false);
        StyleConstants.setBold(styleNgreenB, true);
        StyleConstants.setFontFamily(styleNgreenB, "Georgia");
        StyleConstants.setFontSize(styleNgreenB, 12);
        StyleConstants.setBackground(styleNgreenB, Color.WHITE);
        StyleConstants.setForeground(styleNgreenB, Color.GREEN.darker());
        styles[15] = styleNgreenB; 
        Style styleNgreenBI = doc.addStyle("normalGreenBoldItalic", null);
        StyleConstants.setItalic(styleNgreenBI, true);
        StyleConstants.setBold(styleNgreenBI, true);
        StyleConstants.setFontFamily(styleNgreenBI, "Georgia");
        StyleConstants.setFontSize(styleNgreenBI, 12);
        StyleConstants.setBackground(styleNgreenBI, Color.WHITE);
        StyleConstants.setForeground(styleNgreenBI, Color.GREEN.darker());
        styles[16] = styleNgreenBI;    
        Style styleNorange = doc.addStyle("normalOrange", null);
        StyleConstants.setItalic(styleNorange, false);
        StyleConstants.setBold(styleNorange, false);
        StyleConstants.setFontFamily(styleNorange, "Georgia");
        StyleConstants.setFontSize(styleNorange, 12);
        StyleConstants.setBackground(styleNorange, Color.WHITE);
        StyleConstants.setForeground(styleNorange, Color.ORANGE.darker());
        styles[17] = styleNorange;            
        Style styleNorangeI = doc.addStyle("normalOrangeItalic", null);
        StyleConstants.setItalic(styleNorangeI, true);
        StyleConstants.setBold(styleNorangeI, false);
        StyleConstants.setFontFamily(styleNorangeI, "Georgia");
        StyleConstants.setFontSize(styleNorangeI, 12);
        StyleConstants.setBackground(styleNorangeI, Color.WHITE);
        StyleConstants.setForeground(styleNorangeI, Color.ORANGE.darker());
        styles[18] = styleNorangeI;            
        Style styleNorangeB = doc.addStyle("normalOrangeBold", null);
        StyleConstants.setItalic(styleNorangeB, false);
        StyleConstants.setBold(styleNorangeB, true);
        StyleConstants.setFontFamily(styleNorangeB, "Georgia");
        StyleConstants.setFontSize(styleNorangeB, 12);
        StyleConstants.setBackground(styleNorangeB, Color.WHITE);
        StyleConstants.setForeground(styleNorangeB, Color.ORANGE.darker().darker().darker().darker());
        styles[19] = styleNorangeI;            
        Style styleNorangeBI = doc.addStyle("normalOrangeBoldItalic", null);
        StyleConstants.setItalic(styleNorangeBI, true);
        StyleConstants.setBold(styleNorangeBI, true);
        StyleConstants.setFontFamily(styleNorangeBI, "Georgia");
        StyleConstants.setFontSize(styleNorangeBI, 12);
        StyleConstants.setBackground(styleNorangeBI, Color.WHITE);
        StyleConstants.setForeground(styleNorangeBI, Color.ORANGE.darker());
        styles[20] = styleNorangeBI;            
        Style styleHB = doc.addStyle("highliteBold", null);
        StyleConstants.setItalic(styleHB, false);
        StyleConstants.setBold(styleHB, true);
        StyleConstants.setFontFamily(styleHB, "Georgia");
        StyleConstants.setFontSize(styleHB, 12);
        StyleConstants.setBackground(styleHB, Color.YELLOW.brighter());
        StyleConstants.setForeground(styleHB, Color.BLACK);
        styles[21] = styleHB;
        Style styleQT = doc.addStyle("quotedNormal", null);
        StyleConstants.setItalic(styleQT, false);
        StyleConstants.setBold(styleQT, false);
        StyleConstants.setFontFamily(styleQT, "Georgia");
        StyleConstants.setFontSize(styleQT, 12);
        StyleConstants.setBackground(styleQT, Color.WHITE);
        StyleConstants.setForeground(styleQT, Color.MAGENTA.darker().darker());
        styles[22] = styleQT;
        Style styleRU = doc.addStyle("RedUnderLined", null);
        StyleConstants.setItalic(styleRU, false);
        StyleConstants.setBold(styleRU, false);
        StyleConstants.setUnderline(styleRU, true);
        StyleConstants.setFontFamily(styleRU, "Georgia");
        StyleConstants.setFontSize(styleRU, 12);
        StyleConstants.setBackground(styleRU, Color.WHITE);
        StyleConstants.setForeground(styleRU, Color.RED);
        styles[23] = styleRU;
        return(doc);
        
 
    }
    private void setTabLabels(String title, int count) {
        for (int i=0;i<jTabbedPane_Reader.getTabCount();i++) {
            if(jTabbedPane_Reader.getTitleAt(i).contains(title)) {
                jTabbedPane_Reader.setTitleAt(i, title+" ("+count+")");
            }
        }
    }
    
    protected void createDocumentFromButtonAction(StyledDocument doc, int id, String type) throws BadLocationException {
        //Get Selection Data from Table
        String sql = searchMap.get(type+"Name")+id;
        ArrayList dialogarray = iface.getDBquery(sql);
        Map dataMap = (Map) dialogarray.get(0);
        String classname = dataMap.get("ClassName").toString();
        String name = dataMap.get("Name").toString();
        // Insert Header
        doc.insertString(doc.getLength(), "\t"+classname+"."+name+"\n\n", styles[0]);
        if(typeMap.get(type) < 3) {
            //Get Description
            sql = searchMap.get(type+"Header")+name+"\' and class.name = \'"+classname+"\'";
            ArrayList infoArray = iface.getDBquery(sql);
            dataMap = (Map) infoArray.get(0);
            doc.insertString(doc.getLength(), "\tDescription:", styles[3]);
            doc.insertString(doc.getLength(), "   ", styles[1]);
            if(dataMap.get("Description") != null) {
                doc.insertString(doc.getLength(), dataMap.get("Description").toString(), styles[5]);            
            }
        }
        if(typeMap.get(type) < 2) {
            //Get Priority
            doc.insertString(doc.getLength(), "\n\tPriority:", styles[3]);
            doc.insertString(doc.getLength(), "   ", styles[1]);
            if(dataMap.get("Priority") != null) {
                doc.insertString(doc.getLength(), dataMap.get("Priority").toString(), styles[5]);            
            }
            doc.insertString(doc.getLength(), "\n\tParent:", styles[3]);
            doc.insertString(doc.getLength(), "   ", styles[1]);
            if(dataMap.get("Parent") != null) {
                if(Integer.valueOf(dataMap.get("Parent").toString()) > 0) {
                    doc = insertEventButton(doc, Integer.valueOf(dataMap.get("Parent").toString()));
                } else
                if(Integer.valueOf(dataMap.get("Parent").toString()) == 0) {
                                    doc.insertString(doc.getLength(), "Top", styles[5]);
                }
            }
        }
        // Get Data
        if(typeMap.get(type) == 2) {
            doc.insertString(doc.getLength(), "\n\n", styles[1]);
            sql = searchMap.get("FormalParms").toString()+" WHERE rulemethod.name = '"+name+"' and class.name = '"+classname+"')";
            ArrayList results = iface.getDBquery(sql);
            if(!results.isEmpty()) {
                doc = insertFormalParms(doc, results);            
            }
        }
        sql = searchMap.get(type+"Code").toString()+name+"' and class.name = '"+classname+"' "+searchMap.get(type+"CodeOrderBy").toString();
        doc.insertString(doc.getLength(), "\n\n\t_______________________________________________________________________\n\n", styles[3]); 
        updateDocument(doc, iface.getDBquery(sql), 0, typeMap.get(type));
    }        
    protected void createDocumentFromList(String type, String name) throws BadLocationException {
        String classname = jComboBox_Class.getSelectedItem().toString();
        String sql = searchMap.get(type+"Code")+name+"' and class.name = '"+classname+"' "+searchMap.get(type+"CodeOrderBy");        
        updateDocument(view, iface.getDBquery(sql), 0, 0);
    }
    protected void createDocumentFromResultsTable(StyledDocument doc) throws BadLocationException {
        //Get Selection Data from Table
        TableModel model = jTable_Results.getModel();
        int selrow = jTable_Results.getSelectedRow();
        String type = model.getValueAt(selrow, 3).toString();
        String classname = model.getValueAt(selrow, 0).toString();
        String name = model.getValueAt(selrow, 1).toString();
        int order = Integer.valueOf(model.getValueAt(selrow, 2).toString());
        int typeID = typeMap.get(type);
        doc = createEventHeader(doc, typeID, type, classname, name);
        // Get Data
        String sql = searchMap.get(type+"Code")+name+"' and class.name = '"+classname+"' "+searchMap.get(type+"TableOrderBy");
        updateDocument(doc, iface.getDBquery(sql), order, typeID);        
    }
    protected void createTextPaneReaderPopupMenu() {
        JPopupMenu jPopupMenu = new JPopupMenu();
        String[] tableItemNames = {"Print", "Copy"};
	JMenuItem[] tableMenuItems = new JMenuItem[tableItemNames.length];
        for (int i=0;i < tableItemNames.length;i++){
	    tableMenuItems[i] = new JMenuItem(tableItemNames[i]);
	}
	// Customize Items
	Color itemBGColor = colorMap.get("menu");
	Color itemFGColor = Color.BLACK;
        for (int i=0;i < tableMenuItems.length;i++){
	    jPopupMenu.add(tableMenuItems[i]);
	    tableMenuItems[i].setBackground(itemBGColor);
	    tableMenuItems[i].setForeground(itemFGColor);
	}
	// Action and mouse listener support
	jPopupMenu = new JPopupMenu();
        for (int i=0;i < tableMenuItems.length;i++){
            if(!tableItemNames[i].equals("_")){
                jPopupMenu.add(tableMenuItems[i]);
	    } else {
		jPopupMenu.addSeparator();
	    }
	}	
        jPopupMenu.setBackground(itemBGColor);
	jPopupMenu.setForeground(itemFGColor);
	jPopupMenu.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
	jPopupMenu.setFont(fontMap.get("GA10B"));
	jTextPane_Reader.add(jPopupMenu);
	ActionListener jTable1PopupMenuAL = new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		jTextPane_ActionPerformed(evt);
	    }
	};	
        jTextPane_Reader.setComponentPopupMenu(jPopupMenu);
	enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	for (int i=0;i < tableMenuItems.length;i++){
	    tableMenuItems[i].addActionListener(jTable1PopupMenuAL);
	}	    
    }    
    protected void createTextPaneViewerPopupMenu() {
        JPopupMenu jPopupMenu = new JPopupMenu();
        String[] tableItemNames = {"Print"};
	JMenuItem[] tableMenuItems = new JMenuItem[tableItemNames.length];
        for (int i=0;i < tableItemNames.length;i++){
	    tableMenuItems[i] = new JMenuItem(tableItemNames[i]);
	}
	// Customize Items
	Color itemBGColor = colorMap.get("menu");
	Color itemFGColor = Color.BLACK;
        for (int i=0;i < tableMenuItems.length;i++){
	    jPopupMenu.add(tableMenuItems[i]);
	    tableMenuItems[i].setBackground(itemBGColor);
	    tableMenuItems[i].setForeground(itemFGColor);
	}
	// Action and mouse listener support
	jPopupMenu = new JPopupMenu();
        for (int i=0;i < tableMenuItems.length;i++){
            if(!tableItemNames[i].equals("_")){
                jPopupMenu.add(tableMenuItems[i]);
	    } else {
		jPopupMenu.addSeparator();
	    }
	}	
        jPopupMenu.setBackground(itemBGColor);
	jPopupMenu.setForeground(itemFGColor);
	jPopupMenu.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
	jPopupMenu.setFont(fontMap.get("GA10B"));
	jTextPane_Viewer.add(jPopupMenu);
	ActionListener jTable1PopupMenuAL = new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		jTextPane_ActionPerformed(evt);
	    }
	};	
        jTextPane_Viewer.setComponentPopupMenu(jPopupMenu);
	enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	for (int i=0;i < tableMenuItems.length;i++){
	    tableMenuItems[i].addActionListener(jTable1PopupMenuAL);
	}	    
    }        
    protected StyledDocument createEventHeader(StyledDocument doc, int typeID, String type, String classname, String name) throws BadLocationException {
        // Insert Header
        doc.insertString(doc.getLength(), "\t"+classname+"."+name+"\n\n", styles[0]);
        String[] sqls = {
            "select mess.parent as \"Parent\", mess.priority as \"Priority\", mess.description as \"Description\" from mess join class on class.class = mess.class where mess.name = \'"+name+"\' and class.name = \'"+classname+"\'",
            "select mess.parent as \"Parent\", mess.priority as \"Priority\", mess.description as \"Description\" from mess join class on class.class = mess.class where mess.name = \'"+name+"\' and class.name = \'"+classname+"\'",
            "select rulemethod.description as \"Description\", rulemethod.whereused as \"WhereUsed\" from rulemethod join class on class.class = rulemethod.xclass where rulemethod.name = \'"+name+"\' and class.name = \'"+classname+"\'",
        };
        Map infoMap = null;
        if(typeID < 3) {
            ArrayList infoArray = iface.getDBquery(sqls[typeID]);
            infoMap = (Map) infoArray.get(0);
            doc.insertString(doc.getLength(), "\tDescription:", styles[3]);
            doc.insertString(doc.getLength(), "   ", styles[1]);
            if(infoMap.get("Description") != null) {
                doc.insertString(doc.getLength(), infoMap.get("Description").toString(), styles[5]);            
            }
        }
        if(typeID == 2) {
            String[] whereUsed = {"Event Analysis", "Gateway Analysis", "Event & Gateway Analysis"};
            if(infoMap.get("WhereUsed") != null) {
                doc.insertString(doc.getLength(), "\n\tWhere Used:", styles[3]);
                doc.insertString(doc.getLength(), "   ", styles[1]);
                doc.insertString(doc.getLength(), whereUsed[(Integer.valueOf(infoMap.get("WhereUsed").toString()) - 1)], styles[5]);
            }
            String sql = searchMap.get("FormalParms").toString()+" WHERE rulemethod.name = '"+name+"' and class.name = '"+classname+"')";
            ArrayList results = iface.getDBquery(sql);
            if(!results.isEmpty()) {
                doc.insertString(doc.getLength(), "\n\n", styles[1]);
                doc = insertFormalParms(doc, results);
            }
            
        }
        if(typeID < 2) {
            doc.insertString(doc.getLength(), "\n\tPriority:", styles[3]);
            doc.insertString(doc.getLength(), "   ", styles[1]);
            if(infoMap.get("Priority") != null) {
                doc.insertString(doc.getLength(), infoMap.get("Priority").toString(), styles[5]);            
            }
            doc.insertString(doc.getLength(), "\n\tParent:", styles[3]);
            doc.insertString(doc.getLength(), "   ", styles[1]);
            if(infoMap.get("Parent") != null) {
                if(Integer.valueOf(infoMap.get("Parent").toString()) > 0) {
                    doc = insertEventButton(doc, Integer.valueOf(infoMap.get("Parent").toString()));
                } else
                if(Integer.valueOf(infoMap.get("Parent").toString()) == 0) {
                                    doc.insertString(doc.getLength(), "Top", styles[5]);
                }
            }        
        }
        doc.insertString(doc.getLength(), "\n\n\t_______________________________________________________________________\n\n", styles[3]);
        return(doc);
    }
    protected StyledDocument insertBrokeButton(StyledDocument doc, String text) throws BadLocationException {
        Style styleBTN = doc.addStyle("eyeButton", null);
        final JButton dlgButton = new JButton();
        dlgButton.setFont(fontMap.get("GA10B"));
        dlgButton.setText(text);
        dlgButton.setBackground(Color.RED);
        dlgButton.setForeground(Color.WHITE);
        dlgButton.setToolTipText("Empty Reference");
        dlgButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));     
        StyleConstants.setComponent(styleBTN, dlgButton);
        doc.insertString(doc.getLength(), " ", styleBTN);
        doc.insertString(doc.getLength(), "   ", styles[1]);
        return(doc);
    }      
    protected StyledDocument insertDialogButton(StyledDocument doc, int dialogid) throws BadLocationException {
        String sql = "select class.name as \"ClassName\", dialog.dialogname as \"DialogName\" from dialog join class on class.class = dialog.class where dialog.dialog = "+dialogid; 
        ArrayList dialogList = iface.getDBquery(sql);
        Map dialogMap = (Map) dialogList.get(0);
        Style styleBTN = doc.addStyle("eyeButton", null);
        final JButton dlgButton = new JButton();
        dlgButton.setFont(fontMap.get("GA10B"));
        dlgButton.setText(dialogMap.get("ClassName")+"."+dialogMap.get("DialogName"));
        dlgButton.setBackground(colorMap.get("skyblue"));
        dlgButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        dlgButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    String[] dlginfo;
                    if(dlgButton.getText().contains(".")) {
                        dlginfo = dlgButton.getText().split("\\.");
                    } else {
                        return;
                    }
                    String sql = "select dialog.dialog as \"DialogID\" from dialog join class on class.class = dialog.class where dialog.dialogname = \'"+dlginfo[1]+"\' and class.name = \'"+dlginfo[0]+"\'";
                    ArrayList dlgarray = iface.getDBquery(sql);
                    Map dlgMap = (Map) dlgarray.get(0);
                    int dialogid = Integer.valueOf(dlgMap.get("DialogID").toString());
                    jButton_dlgButtonActionPerformed(evt, dialogid);
                } catch (BadLocationException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });        
        StyleConstants.setComponent(styleBTN, dlgButton);
        doc.insertString(doc.getLength(), " ", styleBTN);
        doc.insertString(doc.getLength(), "   ", styles[1]);
        return(doc);
    }    
    protected StyledDocument insertEventButton(StyledDocument doc, int eventid) throws BadLocationException {
        String sql = "select class.name as \"ClassName\", mess.name as \"EventName\" from mess join class on class.class = mess.class where mess.mess = "+eventid; 
        ArrayList eventList = iface.getDBquery(sql);
        Map eventMap = (Map) eventList.get(0);
        Style styleBTN = doc.addStyle("eyeButton", null);
        final JButton evtButton = new JButton();
        evtButton.setFont(fontMap.get("GA10B"));
        evtButton.setText(eventMap.get("ClassName")+"."+eventMap.get("EventName"));
        evtButton.setBackground(Color.GREEN);
        evtButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        evtButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    String[] evtinfo = evtButton.getText().split("\\.");
                    String sql = "select mess as \"EventID\" from mess join class on class.class = mess.class where mess.name = \'"+evtinfo[1]+"\' and class.name = \'"+evtinfo[0]+"\'";
                    ArrayList evtarray = iface.getDBquery(sql);
                    Map evtMap = (Map) evtarray.get(0);
                    int eventid = Integer.valueOf(evtMap.get("EventID").toString());
                    jButton_evtButtonActionPerformed(evt, eventid);
                } catch (BadLocationException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });        
        StyleConstants.setComponent(styleBTN, evtButton);
        doc.insertString(doc.getLength(), " ", styleBTN);
        doc.insertString(doc.getLength(), "   ", styles[1]);
        return(doc);
    }
    protected StyledDocument insertFormalParms(StyledDocument doc, ArrayList results) {
        try {
            Style styleTBL = doc.addStyle("docTable", null);
            String data[][] = {};
            String col[] = {"Order", "Attribute", "Passed By"};
            DefaultTableModel model = new DefaultTableModel(data,col);
            final JTable docTable = new JTable(model);
            docTable.setFont(fontMap.get("GA10B"));
            docTable.setForeground(Color.BLUE);
            String[] passby = {"ByValue", "ByResult"};
            for (int i = 0; i < results.size(); i++) {
                Map resultMap = (Map) results.get(i);
                Object[] resultObj = new Object[]{resultMap.get("ORDER").toString(), resultMap.get("ATTRIBUTE").toString(), passby[Integer.valueOf(resultMap.get("PASSBY").toString())]};
                model.addRow(resultObj);
            }
            StyleConstants.setComponent(styleTBL, docTable);
            doc.insertString(doc.getLength(), "\tFormal Params:\n\n\t", styles[3]);
            doc.insertString(doc.getLength(), ".", styleTBL);
            doc.insertString(doc.getLength(), "\n\n", styles[3]);     
            return doc;
        } catch (BadLocationException ex) {
            return(doc);
        }
    }
    protected StyledDocument insertMethodButton(StyledDocument doc, int methodid) throws BadLocationException {
        String sql = "select class.name as \"ClassName\", rulemethod.name as \"MethodName\" from rulemethod join class on class.class = rulemethod.xclass where rulemethod.methodid = "+methodid; 
        ArrayList methodList = iface.getDBquery(sql);
        Map methodMap = (Map) methodList.get(0);
        Style styleBTN = doc.addStyle("eyeButton", null);
        final JButton mthButton = new JButton();
        mthButton.setFont(fontMap.get("GA10B"));
        mthButton.setText(methodMap.get("ClassName")+"."+methodMap.get("MethodName"));
        mthButton.setBackground(Color.ORANGE);
        mthButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        mthButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    String[] mthinfo;
                    if(mthButton.getText().contains(".")) {
                        mthinfo = mthButton.getText().split("\\.");
                    } else {
                        return;
                    }
                    String sql = "select rulemethod.methodid as \"MethodID\" from rulemethod join class on class.class = rulemethod.xclass where rulemethod.name = \'"+mthinfo[1]+"\' and class.name = \'"+mthinfo[0]+"\'";
                    ArrayList mtharray = iface.getDBquery(sql);
                    Map mthMap = (Map) mtharray.get(0);
                    int methodid = Integer.valueOf(mthMap.get("MethodID").toString());
                    jButton_mthButtonActionPerformed(evt, methodid);
                } catch (BadLocationException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });        
        StyleConstants.setComponent(styleBTN, mthButton);
        doc.insertString(doc.getLength(), " ", styleBTN);
        doc.insertString(doc.getLength(), "   ", styles[1]);
        return(doc);
    }    
    protected void updateTable(ArrayList tabledata, String type) {
        if(tabledata == null) return;
        DefaultTableModel model = (DefaultTableModel) sorter.getTableModel();
        for(int i=0;i < tabledata.size();i++) {
            Map dataMap = (Map) tabledata.get(i);
            Object[] dataObj = new Object[]{
                dataMap.get("ClassName").toString(),
                dataMap.get("Name").toString(),
                dataMap.get("LineNumber").toString(),
                type
            };
            model.addRow(dataObj);       
        }
    }
    protected void updateDocument(StyledDocument doc, ArrayList results, int order, int type) throws BadLocationException {
        int indentcount = 0;
        boolean methodcall = false;
        boolean dialogcall = false;
        for(int i=0;i < results.size();i++) {
            Style style = styles[1];
            boolean makebold = false;
            boolean comment = false;
            Map lineMap = (Map) results.get(i);
            int linenumber = Integer.valueOf(lineMap.get("LineNumber").toString());
            style = styles[3];
            if(order == linenumber) style = styles[21];
            doc.insertString(doc.getLength(), linenumber+"\t", style);

            String oper = lineMap.get("Operator").toString();
            if(oper.equals("EndIf") || oper.equals("DoEnd")) {indentcount--;}
            for(int b=0;b < indentcount;b++) {
                doc.insertString(doc.getLength(), "        ", styles[1]);
            }
            
            //Operator
            if(oper.equals("Call")) {
                methodcall = true;
            } else if (oper.equals("Dialog")) {
                dialogcall = true;
            }
            if(jComboBox_Fields.getSelectedItem().toString().equals("Operator") && oper.equals("Comment")&& oper.contains(jTextField_SearchString.getText())) {style = styles[15]; makebold = true;comment = true;} else
            if(oper.equals("Comment")) {style = styles[15];comment = true;} else
            if(jComboBox_Fields.getSelectedItem().toString().equals("Operator") && oper.contains(jTextField_SearchString.getText())) {style = styles[7]; makebold = true;comment = true;}
            else {style = styles[5]; }
            doc.insertString(doc.getLength(), oper+"   ", style);
            //Operand1
            if(lineMap.get("Operand1") != null) {
                String opr1 = lineMap.get("Operand1").toString();
                if(methodcall && opr1.contains(".")) {                    
                    methodcall = false;
                    String[] classANDname = opr1.split("\\.");
                    String sql = "select methodid as \"MethodID\" from rulemethod join class on class.class = rulemethod.xclass where class.name = \'"+classANDname[0]+"\' and rulemethod.name = \'"+classANDname[1]+"\'";
                    ArrayList methodresult = iface.getDBquery(sql);
                    if(methodresult.size() > 0) {
                    Map methodMap = (Map) methodresult.get(0);
                    doc = insertMethodButton(doc, Integer.valueOf(methodMap.get("MethodID").toString()));
                    } else {
                        doc = insertBrokeButton(doc, opr1);
                    }
                } else if(dialogcall && opr1.contains(".")) {
                    dialogcall = false;
                    String[] classANDname = opr1.split("\\.");
                    String sql = "select dialog as \"DialogID\" from dialog join class on class.class = dialog.class where class.name = \'"+classANDname[0]+"\' and dialog.dialogname = \'"+classANDname[1]+"\'";
                    ArrayList dialogresult = iface.getDBquery(sql);
                    if(dialogresult.size() > 0) {
                    Map dialogMap = (Map) dialogresult.get(0);
                    doc = insertDialogButton(doc, Integer.valueOf(dialogMap.get("DialogID").toString()));
                    } else {
                        doc = insertBrokeButton(doc, opr1);
                    }
                } else {
                    if(jComboBox_Fields.getSelectedItem().toString().equals("Operator") && oper.equals("Comment")&& oper.contains(jTextField_SearchString.getText())) {style = styles[15]; makebold = true;} else
                    if(jComboBox_Fields.getSelectedItem().toString().equals("Operand1") && opr1.contains(jTextField_SearchString.getText())) {style = styles[3];} else
                    if(makebold) {style = styles[3];} else
                    if(comment) {style = styles[13];} else
                    if(opr1.startsWith("@")) {style = styles[23];}
                    else { style = styles [1]; }
                    doc.insertString(doc.getLength(), opr1, style);
                    doc.insertString(doc.getLength(), "   ", styles[1]);
                }
            }
            //Operand2
            if(lineMap.get("Operand2") != null) {
                String opr2 = lineMap.get("Operand2").toString();
                try {
                    Integer.parseInt(opr2);
                    style = styles[11];
                } catch(NumberFormatException ex) {
                    if(jComboBox_Fields.getSelectedItem().toString().equals("Operator") && oper.equals("Comment")&& oper.contains(jTextField_SearchString.getText())) {style = styles[15]; makebold = true;} else
                    if(jComboBox_Fields.getSelectedItem().toString().equals("Operand2") && opr2.contains(jTextField_SearchString.getText())) {style = styles[3];} else
                    if(makebold) {style = styles[3];} else
                    if(comment) {style = styles[13];}  else 
                    if(opr2.startsWith("\"") && opr2.endsWith("\"")) {style = styles[22];} else
                    if(opr2.startsWith("@")) {style = styles[23];}
                    else { style = styles [1]; }
                }
                doc.insertString(doc.getLength(), opr2, style);
                doc.insertString(doc.getLength(), "   ", styles[1]);
            }
            if(lineMap.get("Relate") != null) {
                if(comment) {style = styles[13];}  else {
                    style = styles[18];
                }
                doc.insertString(doc.getLength(), lineMap.get("Relate").toString(), style);
                doc.insertString(doc.getLength(), "   ", styles[1]);
            }
            if(lineMap.get("EventID") != null) {
                if(Integer.valueOf(lineMap.get("EventID").toString()) > 0) {
                    doc = insertEventButton(doc, Integer.valueOf(lineMap.get("EventID").toString()));
                } else if(Integer.valueOf(lineMap.get("EventID").toString()) < 0) {
                    doc.insertString(doc.getLength(), "@EventName", styles[23]);
                    doc.insertString(doc.getLength(), "   ", styles[1]);
                }
            }
            if(lineMap.get("AlertID") != null && type < 2) {
                if(Integer.valueOf(lineMap.get("AlertID").toString()) > 0) {
                    String sql = "select name as \"AlertName\" from alertid where alertid = "+Integer.valueOf(lineMap.get("AlertID").toString());
                    ArrayList alertList = iface.getDBquery(sql);
                    Map alertMap = (Map) alertList.get(0);
                    style = styles[23];
                    doc.insertString(doc.getLength(), alertMap.get("AlertName").toString(), style);                    
                } else
                if(Integer.valueOf(lineMap.get("AlertID").toString()) < 0) {
                    doc.insertString(doc.getLength(), "@AlertName", styles[23]);                    
                }
            }
            doc.insertString(doc.getLength(), "\n", styles[1]);
            if(oper.equals("If") || oper.equals("Do")) {indentcount++;}            
        }
            Runnable runnable = new Runnable() {
                public void run() {


                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if(jScrollPane6.isVisible()) {
                        jScrollPane6.getVerticalScrollBar().setValue(jScrollPane6.getVerticalScrollBar().getMinimum());			
                    } else
                    if(jScrollPane2.isVisible()) {
                        jScrollPane2.getVerticalScrollBar().setValue(jScrollPane2.getVerticalScrollBar().getMinimum());
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.setPriority(Thread.NORM_PRIORITY -1);
            thread.start();
    }    

    private void setDatabasesFromProperties() throws Exception {
    Properties props = iface.getProperties();
    Enumeration e = props.propertyNames();
    Map<String, String> tmpMap = new HashMap<String, String>();
    while(e.hasMoreElements()) {
        String key = e.nextElement().toString();
        if(key.startsWith("DB.")) {
            tmpMap.put(key, props.getProperty(key));
        }
    }
    for (int i=0;i < (tmpMap.size() / 7);i++) {
        Map<String, String> dbMap = new HashMap<String, String>();
        dbMap.put("Category", tmpMap.get("DB."+i+".Category"));
        dbMap.put("System", tmpMap.get("DB."+i+".System"));
        dbMap.put("Server", tmpMap.get("DB."+i+".Server"));
        dbMap.put("Port", tmpMap.get("DB."+i+".Port"));
        dbMap.put("Sid", tmpMap.get("DB."+i+".Sid"));
        dbMap.put("User", tmpMap.get("DB."+i+".User"));
        dbMap.put("Pass", tmpMap.get("DB."+i+".Pass"));
        databases.add(dbMap);
        
        jMenu_Databases.add(jMenuItem1);
    }      
}
    private void printDocument(JTextPane textpane) {
        StyledDocumentPrinter sdPrinter = new StyledDocumentPrinter(textpane.getStyledDocument());
        PrintRequestAttributeSet attributeSet = null;
        sdPrinter.printDocument(attributeSet);
    }
    private void copyDocument(final JTextPane textpane) {
        Clipboard clipboard = this.getToolkit().getSystemClipboard();
                Transferable xfer = new Transferable() {
                    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException ,IOException {
                        if (flavor.equals(DataFlavor.stringFlavor)) {
                            String text = null;
                            if(textpane.getSelectedText() != null) {
                                text = textpane.getSelectedText();
                            } else {
                                text = textpane.getText();
                            }
                            return(text);
                        }
                        throw new UnsupportedFlavorException(flavor);
                    }
                    public DataFlavor[] getTransferDataFlavors() {
                        return new DataFlavor[] { DataFlavor.stringFlavor };
                    }
                    public boolean isDataFlavorSupported(DataFlavor flavor) {
                        return flavor.equals(DataFlavor.stringFlavor);
                    }
                };
        clipboard.setContents(xfer, null);     
    }
    
    //Internal Classes
    class CustomCellRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null;
    GradientPaint gp = null;    
    public CustomCellRenderer() {
        super();
        setOpaque(false);
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));
    }   
    @Override
    public Component getTableCellRendererComponent(
         JTable table,
         Object value,
         boolean isSelected,
         boolean hasFocus,
         int row,
         int column
         
         ) {
        Component comp = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        Color tabColor = colorMap.get("skyblue");
        String type = table.getModel().getValueAt(row, 3).toString();
        if(type.equals("Event")) {tabColor = Color.GREEN;}
        else if(type.equals("Gateway")) {tabColor = new Color(0, 255, 127);}
        else if(type.equals("Method")) {tabColor = Color.ORANGE;}
        
        ImageIcon icon = new ImageIcon();
        setIcon(icon);
        setIconTextGap(6);        
        Rectangle rect = table.getCellRect(row, column, isSelected);
        paintingRect = rect;
        setFont(fontMap.get("GA11"));
        setForeground(colorMap.get("black"));
        int[] sels = table.getSelectedRows();
        TableModel model = table.getModel();
        if(isSelected) {
            if(value instanceof String) setFont(fontMap.get("GA12B"));
            if(rect != null) {
                    view = (StyledDocument) jTextPane_Viewer.getDocument();
                    if(sels[0] == row ||sels.length == 1) {            
                        try {
                            gp = new GradientPaint(0, 0, tabColor.darker(), 0, rect.height / 2, tabColor.brighter());
                            view = clearViewer(view);
                            createDocumentFromResultsTable(setStyles(view));
                        } catch (BadLocationException ex) {
                            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else if(sels[table.getSelectedRowCount() - 1] == row) {
                        try {
                            gp = new GradientPaint(0, 0, tabColor.brighter(), 0, rect.height / 2, tabColor.darker());
                            view = clearViewer(view);
                        } catch (BadLocationException ex) {
                            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        gp = new GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.brighter());
                    }
                }
        } else if(!isSelected) {
            if(rect != null) {
                gp = new GradientPaint(0,0,tabColor.brighter(),(rect.width * 7)/8,0,tabColor.darker());
            }
        }
        int[] colAlignments = new int[]{2,2,2,0};
        setHorizontalAlignment(colAlignments[column]);
        return(comp);
    }
    public GradientPaint getColumnGradient() {
        return gp;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
        Graphics2D g2 = (Graphics2D)g;
        g2.setPaint(gp);
        g2.fillRect( 0, 0, rect.width, rect.height ); 
        super.paintComponent(g);
    }
 }  
    class CustomHeaderRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null,lpr = null; 
    JTableHeader header = null;
    GradientPaint gp = null, hoverGradient, columnGradient;
 
    public CustomHeaderRenderer() {
        super();
        setOpaque(false);
        setFont(fontMap.get("GA16B"));
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));                      
    }   
    public void setColumnGradient(GradientPaint gp) {
        this.columnGradient = gp;
    }
    public GradientPaint getColumnGradient() {
        return columnGradient;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
 
        Graphics2D g2 = (Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
 
        super.paintComponent(g);
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int col) {
        int[] colAlignments = new int[]{2,2,2,2,2};
        if(colAlignments != null) setHorizontalAlignment(colAlignments[col]);
        Rectangle rect = table.getTableHeader().getHeaderRect(col);
        gp = new GradientPaint(0, 0, colorMap.get("header").brighter(), 0, rect.height/2, colorMap.get("header"));
        setForeground(colorMap.get("black")); 
        paintingRect = rect;
        setText( value == null ? "" : value.toString() );
        ImageIcon icon = new ImageIcon();
        setIcon(icon);
        setIconTextGap(6);
        return(this);
    }
    }    
    class CustomTreeCellRenderer extends DefaultTreeCellRenderer {

            public CustomTreeCellRenderer() 
            { 
                super(); 
            } 
            @Override
        public Component getTreeCellRendererComponent(
                                                        JTree tree,
                                                        Object value,
                                                        boolean selection, 
                                                        boolean expanded,
                                                        boolean leaf, 
                                                        int row, 
                                                        boolean hasFocus) { 
                super.getTreeCellRendererComponent(tree, value, selection, expanded, leaf, row, hasFocus); 
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)value; 
                setIconAndToolTip(node.getUserObject(), tree, expanded, leaf, selection);
                if(selection) {
                    boolean isroot = false;
                    String ntext = "";
                    if(!node.isRoot()){
                        DefaultMutableTreeNode pnode = (DefaultMutableTreeNode) node;
                        while(!isroot) {
                            ntext = pnode.toString()+ntext;
                            if(pnode.isRoot()) {
                                isroot = true;
                            } else {
                                pnode = (DefaultMutableTreeNode) pnode.getParent();
                            }
                        }
                        DefaultMutableTreeNode treenode = treeMap.get(ntext);
                        if(treenode == null && ntext.endsWith("/")) {
                            treeMap.put(ntext, (DefaultMutableTreeNode)node);
                            createjTreeNode(tree, node, ntext);
                            tree.revalidate();
                        }             
                    } 
                }
                    
                return(this);
        }
        private void setIconAndToolTip(Object obj, JTree tree, boolean expanded, boolean leaf, boolean selected) {
            setFont(fontMap.get("GA10B"));
            tree.setBackground(colorMap.get("menu"));
            setForeground(colorMap.get("black"));
            setBackgroundNonSelectionColor(colorMap.get("menu"));
            setBackgroundSelectionColor(colorMap.get("header"));
            Icon icon = null;
            if(obj instanceof String) {
                
                String str = (String)(obj);
                if(str.equals(iface.getSVNroot())) {
                    icon = new ImageIcon(iface.getURL("SVNicon.png"));
                    setIcon(icon);
                } else {
                            //Directories
                            if (str.endsWith("/") && selected){
                                icon = new ImageIcon(iface.getURL("folder_open.gif"));			
                            } else if(str.endsWith("/") && expanded){
                                icon = new ImageIcon(iface.getURL("folder_open.gif"));
                            } else if(str.endsWith("/")) {
                                icon = new ImageIcon(iface.getURL("folder_close.gif"));
                            //Files
                            } else if(str.contains(".")) {
                                String[] ext = str.toLowerCase().split("\\.");
                                if (ext.length > 0) icon = extMap.get(ext[ext.length - 1]);
                            }
                            if(icon == null) {
                                icon = new ImageIcon(iface.getURL("File.png"));
                            }
                    setIcon(icon);
                }
                BufferedImage bufferedIcon = null;
                try {
                    bufferedIcon = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_BYTE_INDEXED);
                } catch (Exception ex) {
                    
                }
                Graphics g = bufferedIcon.getGraphics();
                iconPaint(g);
            }
        }
        @Override
        public Color getBackgroundNonSelectionColor() {
            return super.getBackgroundNonSelectionColor();
        }
        @Override
        public Color getBackgroundSelectionColor() {
            return super.getBackgroundSelectionColor();
        }
        @Override
        public synchronized DropTarget getDropTarget() {
            return super.getDropTarget();
        }
        public int getNEXT() {
            return NEXT;
        }
        public int getPREVIOUS() {
            return PREVIOUS;
        }
        @Override
        public void setBackgroundNonSelectionColor(Color newColor) {
            super.setBackgroundNonSelectionColor(newColor);
        }
        @Override
        public void setBackgroundSelectionColor(Color newColor) {
            super.setBackgroundSelectionColor(newColor);
        }

        @Override
        public void setForeground(Color fg) {
            super.setForeground(fg);
        }

        public void iconPaint( Graphics g ) {
            g.fillRect( 0, 0, getWidth() - 1, getHeight() - 1 );
            g.setColor(colorMap.get("menu"));
            this.paint(g);
        }
    }       
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ReadPanel;
    private javax.swing.JPanel SearchPanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton_Go;
    private javax.swing.JComboBox jComboBox_Class;
    private javax.swing.JComboBox jComboBox_Fields;
    private javax.swing.JComboBox jComboBox_Operator;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jList_Dialogs;
    private javax.swing.JList jList_Events;
    private javax.swing.JList jList_Methods;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenu jMenu_Databases;
    private javax.swing.JMenu jMenu_File;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JRadioButton jRadioButton_Dialogs;
    private javax.swing.JRadioButton jRadioButton_Events;
    private javax.swing.JRadioButton jRadioButton_Methods;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane_Reader;
    private javax.swing.JSplitPane jSplitPane_SVN;
    private javax.swing.JSplitPane jSplitPane_Search;
    private javax.swing.JTabbedPane jTabbedPane_Main;
    private javax.swing.JTabbedPane jTabbedPane_Reader;
    private javax.swing.JTable jTable_Results;
    private javax.swing.JTextField jTextField_SearchString;
    private javax.swing.JTextPane jTextPane_Reader;
    private javax.swing.JTextPane jTextPane_Viewer;
    private javax.swing.JTree jTreeDIR;
    private javax.swing.JTree jTreeSVN;
    // End of variables declaration//GEN-END:variables
    
}
