
package ruletools;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

public class GUI_Options extends javax.swing.JFrame {
    private Pkg_Interface iface = null;
    Map<String, DefaultMutableTreeNode> nodeMap = new HashMap<String, DefaultMutableTreeNode>();
    public ArrayList databases = new ArrayList();
    public String[] categories = {"Development", "System Test", "Integration Test", "Load Test", "Production"};
    public GUI_Options(Pkg_Interface aface) {
        iface = aface;
        initComponents();
        //Window Listener
	addWindowListener(new WindowAdapter() {
	    @Override
	public void windowClosing(WindowEvent ex) {
                try {
                    setProperties();
                    iface.getConnected();
                } catch (Exception ex1) {
                    Logger.getLogger(GUI_Options.class.getName()).log(Level.SEVERE, null, ex1);
                }
	}
	});  
        try {
            initConfigs();
        } catch (Exception ex) {
            Logger.getLogger(GUI_Options.class.getName()).log(Level.SEVERE, null, ex);
        }
	MouseListener jTree_DBMouseListener = new MouseAdapter() {
	    @Override
            public void mousePressed(MouseEvent mc) {
                int selRow = jTree_DB.getRowForLocation(mc.getX(), mc.getY());
                TreePath selPath = jTree_DB.getPathForLocation(mc.getX(), mc.getY());
                if(selRow != -1) {
                    if(mc.getButton() == MouseEvent.BUTTON1) {
                        if(mc.getClickCount() == 1 && selPath.getPathCount() > 1) {
                            String selection = selPath.toString().split(",")[selPath.getPathCount() - 1].replace("]", "").substring(1);
                            String parent = selPath.getParentPath().toString().split(",")[selPath.getPathCount() - 2].replace("]", "").substring(1);
                            for(int i=0;i < databases.size();i++) {
                                Map<String, String> tmpMap = (Map) databases.get(i);
                                if(tmpMap.get("System").equals(selection) && tmpMap.get("Server").equals(parent)) {
                                    jTextField_OSI_SYSTEM.setText(tmpMap.get("System"));
                                    jTextField_DBport.setText(tmpMap.get("Port"));
                                    jTextField_DBsid.setText(tmpMap.get("Sid"));
                                    jTextField_DBserver.setText(tmpMap.get("Server"));
                                    jComboBox_DBcategory.setSelectedItem(tmpMap.get("Category"));
                                    jTextField_DBuser.setText(tmpMap.get("User"));
                                    jPasswordField_DBpass.setText(tmpMap.get("Pass"));
                                }
                             }
                        } else if(mc.getClickCount() == 2) {
                            String selection = selPath.toString().split(",")[selPath.getPathCount() - 1].replace("]", "").substring(1);
                            String parent = selPath.getParentPath().toString().split(",")[selPath.getPathCount() - 2].replace("]", "").substring(1);
                            for(int i=0;i < databases.size();i++) {
                                Map<String, String> tmpMap = (Map) databases.get(i);
                                if(tmpMap.get("System").equals(selection) && tmpMap.get("Server").equals(parent)) {
                                    jTextField_OSI_SYSTEM.setText(tmpMap.get("System"));
                                    jTextField_DBport.setText(tmpMap.get("Port"));
                                    jTextField_DBsid.setText(tmpMap.get("Sid"));
                                    jTextField_DBserver.setText(tmpMap.get("Server"));
                                    jComboBox_DBcategory.setSelectedItem(tmpMap.get("Category"));
                                    jTextField_DBuser.setText(tmpMap.get("User"));
                                    jPasswordField_DBpass.setText(tmpMap.get("Pass"));
                                }
                             }
                            try {
                                saveDB();
                            } catch (Exception ex) {
                                Logger.getLogger(GUI_Options.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
	};
        jTree_DB.addMouseListener(jTree_DBMouseListener);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jCheckBox_memGUIpos = new javax.swing.JCheckBox();
        jCheckBox_memGUIdim = new javax.swing.JCheckBox();
        jCheckBox_loadlastDB = new javax.swing.JCheckBox();
        jCheckBox_min2tray = new javax.swing.JCheckBox();
        jPanel_DB2 = new javax.swing.JPanel();
        jPanel_DB = new javax.swing.JPanel();
        jLabel_DBheader = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel_DBserver = new javax.swing.JLabel();
        jLabel_DBport = new javax.swing.JLabel();
        jLabel_DBsid = new javax.swing.JLabel();
        jLabel_DBuser = new javax.swing.JLabel();
        jLabel_DBpass = new javax.swing.JLabel();
        jLabel_OSI_SYSTEM = new javax.swing.JLabel();
        jLabel_DBcategroy = new javax.swing.JLabel();
        jTextField_OSI_SYSTEM = new javax.swing.JTextField();
        jComboBox_DBcategory = new javax.swing.JComboBox();
        jTextField_DBserver = new javax.swing.JTextField();
        jTextField_DBport = new javax.swing.JTextField();
        jTextField_DBsid = new javax.swing.JTextField();
        jTextField_DBuser = new javax.swing.JTextField();
        jPasswordField_DBpass = new javax.swing.JPasswordField();
        jButton_DBnew = new javax.swing.JButton();
        jButton_DBdel = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree_DB = new javax.swing.JTree();
        jButton_DBsave = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel9.setText("General");

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(183, 183, 183)
                .add(jLabel9)
                .addContainerGap(191, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel9)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jCheckBox_memGUIpos.setText("Remember GUI position");

        jCheckBox_memGUIdim.setText("Remember GUI dimensions");

        jCheckBox_loadlastDB.setText("Load last used database by default");

        jCheckBox_min2tray.setText("Minimize to tray (if supported)");

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jCheckBox_memGUIpos)
                    .add(jCheckBox_memGUIdim)
                    .add(jCheckBox_loadlastDB)
                    .add(jCheckBox_min2tray))
                .addContainerGap(247, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .add(jCheckBox_memGUIpos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jCheckBox_memGUIdim)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jCheckBox_loadlastDB)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jCheckBox_min2tray)
                .addContainerGap(217, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("     General     ", jPanel1);

        jPanel_DB.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_DBheader.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel_DBheader.setText("Databases");

        org.jdesktop.layout.GroupLayout jPanel_DBLayout = new org.jdesktop.layout.GroupLayout(jPanel_DB);
        jPanel_DB.setLayout(jPanel_DBLayout);
        jPanel_DBLayout.setHorizontalGroup(
            jPanel_DBLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel_DBLayout.createSequentialGroup()
                .add(172, 172, 172)
                .add(jLabel_DBheader)
                .addContainerGap(181, Short.MAX_VALUE))
        );
        jPanel_DBLayout.setVerticalGroup(
            jPanel_DBLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel_DBLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel_DBheader)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_DBserver.setText("DB Server:");

        jLabel_DBport.setText("DB Port:");

        jLabel_DBsid.setText("DB SID:");

        jLabel_DBuser.setText("DB User:");

        jLabel_DBpass.setText("DB Pass:");

        jLabel_OSI_SYSTEM.setText("OSI_SYSTEM:");

        jLabel_DBcategroy.setText("Catagory");

        jComboBox_DBcategory.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Development", "System Test", "Ingetration Test", "Load Test", "Production" }));

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel_OSI_SYSTEM)
                            .add(jLabel_DBcategroy)
                            .add(jLabel_DBserver)
                            .add(jLabel_DBport))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jTextField_DBserver, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextField_OSI_SYSTEM, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .add(jTextField_DBport, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jComboBox_DBcategory, 0, 163, Short.MAX_VALUE)))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel_DBsid)
                            .add(jLabel_DBuser)
                            .add(jLabel_DBpass))
                        .add(28, 28, 28)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jTextField_DBuser, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .add(jTextField_DBsid, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .add(jPasswordField_DBpass, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_OSI_SYSTEM)
                    .add(jTextField_OSI_SYSTEM, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_DBcategroy)
                    .add(jComboBox_DBcategory, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_DBserver)
                    .add(jTextField_DBserver, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_DBport)
                    .add(jTextField_DBport, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_DBsid)
                    .add(jTextField_DBsid, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_DBuser)
                    .add(jTextField_DBuser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_DBpass)
                    .add(jPasswordField_DBpass, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jButton_DBnew.setText("New Database");
        jButton_DBnew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_DBnewActionPerformed(evt);
            }
        });

        jButton_DBdel.setText("Delete Database");
        jButton_DBdel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_DBdelActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jTree_DB);

        jButton_DBsave.setText("Set Database");
        jButton_DBsave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_DBsaveActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel_DB2Layout = new org.jdesktop.layout.GroupLayout(jPanel_DB2);
        jPanel_DB2.setLayout(jPanel_DB2Layout);
        jPanel_DB2Layout.setHorizontalGroup(
            jPanel_DB2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel_DB2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel_DB2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel_DB, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel_DB2Layout.createSequentialGroup()
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 189, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel_DB2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton_DBdel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                            .add(jButton_DBnew, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton_DBsave, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel_DB2Layout.setVerticalGroup(
            jPanel_DB2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel_DB2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel_DB, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel_DB2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel_DB2Layout.createSequentialGroup()
                        .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton_DBnew)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton_DBdel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 23, Short.MAX_VALUE)
                        .add(jButton_DBsave))
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("     Database     ", jPanel_DB2);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 472, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 401, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("     SVN     ", jPanel3);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTabbedPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 477, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jTabbedPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 429, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButton_DBnewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_DBnewActionPerformed
    newDB();
}//GEN-LAST:event_jButton_DBnewActionPerformed

private void jButton_DBdelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_DBdelActionPerformed
    if(jTextField_DBserver != null && jTextField_OSI_SYSTEM != null) {
            try {
                delDB(jTextField_DBserver.getText(), jTextField_OSI_SYSTEM.getText());
            } catch (Exception ex) {
                Logger.getLogger(GUI_Options.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}//GEN-LAST:event_jButton_DBdelActionPerformed

private void jButton_DBsaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_DBsaveActionPerformed
        try {
            saveDB();//GEN-LAST:event_jButton_DBsaveActionPerformed
        } catch (Exception ex) {
            Logger.getLogger(GUI_Options.class.getName()).log(Level.SEVERE, null, ex);
        }
}                                              

    private void newDB() {
        jTextField_DBserver.setText("");
        jTextField_OSI_SYSTEM.setText("");
        jTextField_DBport.setText("1521");
        jTextField_DBsid.setText("");
        jTextField_DBuser.setText("");
        jPasswordField_DBpass.setText("");        
    }
    private void saveDB() throws Exception {
        Properties props = iface.getProperties();
        int index = -1;
        Color color;
        //Get Values
        String server = jTextField_DBserver.getText().toString();
        String system = jTextField_OSI_SYSTEM.getText().toString();
        String category = jComboBox_DBcategory.getSelectedItem().toString();
        String port = jTextField_DBport.getText().toString();
        String sid = jTextField_DBsid.getText().toString();
        String user = jTextField_DBuser.getText().toString();
        char[] pass = jPasswordField_DBpass.getPassword();
        //Return Errors if blank
        boolean errs = false;
        color = Color.RED;
        if(server.length() < 1) {setBGcolor(jTextField_DBserver,color);errs=true;}
        if(system.length() < 1) {setBGcolor(jTextField_OSI_SYSTEM,color);errs=true;}
        if(port.length() < 1) {setBGcolor(jTextField_DBport,color);errs=true;}
        if(sid.length() < 1) {setBGcolor(jTextField_DBsid,color);errs=true;}
        if(user.length() < 1) {setBGcolor(jTextField_DBuser,color);errs=true;}
        if(pass.length < 1) {setBGcolor(jPasswordField_DBpass,color);errs=true;}        
        if(errs) {return;}
        
        //Find if Existing
        for(int i=0;i < databases.size();i++) {
            Map<String, String> tmpMap = (Map) databases.get(i);
            if(tmpMap.get("Server").equals(server) && tmpMap.get("System").equals(system)) {
                index = i;
            }
        }
        //Construct Map
        Map<String, String> dbMap = new HashMap<String, String>(); 
        dbMap.put("Server", server);
        dbMap.put("System", system);
        dbMap.put("Category", category);
        dbMap.put("Port", port);
        dbMap.put("Sid", sid);
        dbMap.put("User", user);
        String password = "";
        for(int i=0;i < pass.length;i++) {
            password += pass[i];
        }
        dbMap.put("Pass", password);
        //Add or Update Array
        if(index < 0) {
            databases.add(dbMap);
        } else {
            databases.set(index, dbMap);
        }      
        String dbURL = "jdbc:oracle:thin:@"+server+":"+port+":"+sid;
        Connection conn = iface.getDB().DbConnect(props.getProperty("dbDriver"), dbURL, user, password);
        props.setProperty("dbURL", dbURL);
        props.setProperty("dbUser", user);
        props.setProperty("dbPass", password);
        iface.setDefaultTitle();
        iface.getGUI().setTitle(iface.getGUI().getTitle()+"   @"+dbURL+"@"+user);
        if(conn != null) {
            if(!conn.isClosed()) {
                color = Color.GREEN;
                iface.setConnection(conn);
            }
        }
        setBGcolor(jTextField_DBserver,color);
        setBGcolor(jTextField_OSI_SYSTEM,color);
        setBGcolor(jTextField_DBport,color);
        setBGcolor(jTextField_DBsid,color);
        setBGcolor(jTextField_DBuser,color);
        setBGcolor(jPasswordField_DBpass,color);       
        setDBtree();
        iface.setProperties(props);
    }    
    private void delDB(String server, String system) throws Exception {
        for(int i=0;i < databases.size();i++) {
            Map<String, String> tmpMap = (Map) databases.get(i);
            if(tmpMap.get("Server").equals(server) && tmpMap.get("System").equals(system)) {
                nodeMap.clear();
                databases.remove(i);               
                Color color = Color.RED;
                setBGcolor(jTextField_DBserver,color);
                setBGcolor(jTextField_OSI_SYSTEM,color);
                setBGcolor(jTextField_DBport,color);
                setBGcolor(jTextField_DBsid,color);
                setBGcolor(jTextField_DBuser,color);
                setBGcolor(jPasswordField_DBpass,color);  
            }            
        }
        createjTreeDBRoot();
        setDBtree();
    }    
    public Properties remDBProperties(Properties props) {
        Enumeration e = props.propertyNames();
        while(e.hasMoreElements()) {
            String key = e.nextElement().toString();
            if(key.startsWith("DB.")) {props.remove(key);}
        }
        return(props);
    }     
    private void setDatabasesFromProperties() throws Exception {
        Properties props = iface.getProperties();
        Enumeration e = props.propertyNames();
        Map<String, String> tmpMap = new HashMap<String, String>();
        while(e.hasMoreElements()) {
            String key = e.nextElement().toString();
            if(key.startsWith("DB.")) {
                tmpMap.put(key, props.getProperty(key));
            }
        }
        for (int i=0;i < (tmpMap.size() / 7);i++) {
            Map<String, String> dbMap = new HashMap<String, String>();
            dbMap.put("Category", tmpMap.get("DB."+i+".Category"));
            dbMap.put("System", tmpMap.get("DB."+i+".System"));
            dbMap.put("Server", tmpMap.get("DB."+i+".Server"));
            dbMap.put("Port", tmpMap.get("DB."+i+".Port"));
            dbMap.put("Sid", tmpMap.get("DB."+i+".Sid"));
            dbMap.put("User", tmpMap.get("DB."+i+".User"));
            dbMap.put("Pass", tmpMap.get("DB."+i+".Pass"));
            databases.add(dbMap);
        }            
    }
    private void setDBtree() {
        if(databases.size() > 0) {
            for (int i=0;i < databases.size();i++) {
                Map<String, String> dbprops = (Map) databases.get(i);
                //Get Category Node
                DefaultMutableTreeNode nodeCat = nodeMap.get(dbprops.get("Category").toString());
                DefaultMutableTreeNode nodeSrv = null;
                DefaultMutableTreeNode nodeSys = null;
                //Server Node
                nodeSrv = nodeMap.get(dbprops.get("Server").toString());
                if(nodeSrv == null) {
                    createjTreeDBNode(jTree_DB, nodeCat, dbprops.get("Server"));
                    nodeSrv = nodeMap.get(dbprops.get("Server").toString());                
                }
                //Get System Node
                nodeSys = nodeMap.get(dbprops.get("System").toString());
                if(nodeSys == null) {
                    createjTreeDBNode(jTree_DB, nodeSrv, dbprops.get("System"));
                    nodeSys = nodeMap.get(dbprops.get("System").toString());   
                }
            }
            jTree_DB.updateUI();
        } else {
            createjTreeDBRoot();
        }
    }
    private void setBGcolor(final Component component, Color color) {
        component.setBackground(color);	    
        Thread thread = new Thread("FadeBGThread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                Color bg;
                int bg_r,bg_g,bg_b;
                while (!component.getBackground().equals(Color.WHITE)) {
                    try {
                        bg = component.getBackground();
                        bg_r=(bg.getRed()*3)/100 +bg.getRed()+1; if(bg_r > 255) bg_r=255;
                        bg_g=(bg.getGreen()*3)/100 +bg.getGreen()+1; if(bg_g > 255) bg_g=255;
                        bg_b=(bg.getBlue()*3)/100 +bg.getBlue()+1; if(bg_b > 255) bg_b=255;                          
                        component.setBackground(new Color(bg_r, bg_g, bg_b));
                        Thread.sleep(33);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        };        
        thread.start();
    }
    private void setProperties() throws Exception {
        Properties props = iface.getProperties();
        props = remDBProperties(props);
        for(int i=0;i < databases.size();i++) {
            Map<String, String> tmpMap = (Map) databases.get(i);
            props.setProperty("DB."+i+".Server", tmpMap.get("Server"));
            props.setProperty("DB."+i+".Port", tmpMap.get("Port"));
            props.setProperty("DB."+i+".Sid", tmpMap.get("Sid"));
            props.setProperty("DB."+i+".User", tmpMap.get("User"));
            props.setProperty("DB."+i+".Pass", tmpMap.get("Pass"));
            props.setProperty("DB."+i+".System", tmpMap.get("System"));
            props.setProperty("DB."+i+".Category", tmpMap.get("Category"));
        }
        props.setProperty("GUI.jCheckBox_memGUIpos", String.valueOf(jCheckBox_memGUIpos.isSelected()));
        props.setProperty("GUI.jCheckBox_memGUIdim", String.valueOf(jCheckBox_memGUIdim.isSelected()));
        props.setProperty("GUI.jCheckBox_loadlastDB", String.valueOf(jCheckBox_loadlastDB.isSelected()));
        props.setProperty("GUI.jCheckBox_min2tray", String.valueOf(jCheckBox_min2tray.isSelected()));
        iface.setProperties(props);
    }
    public void createjTreeDBRoot() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Databases");
        jTree_DB.setLargeModel(true);
        jTree_DB.setBackground((Color) iface.getGUI().getColorMap().get("menu"));
        DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
        jTree_DB.setModel(treeModel);
        jTree_DB.setFont((Font) iface.getGUI().getFontMap().get("GA10B"));
        CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
        jTree_DB.setCellRenderer(custom);
        for(int i=0;i < categories.length;i++) {
            createjTreeDBNode(jTree_DB, rootNode, categories[i]);
        }
        jTree_DB.updateUI();        
    }
    public void createjTreeDBNode(JTree tree,DefaultMutableTreeNode node,String text) {
        CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
        tree.setCellRenderer(custom);
        DefaultMutableTreeNode newnode = new DefaultMutableTreeNode(text);
        nodeMap.put(text, newnode);
        node.add(newnode);
    }     
    private void initConfigs() throws Exception {
        Properties props = iface.getProperties();
        jCheckBox_memGUIpos.setSelected(Boolean.valueOf(props.getProperty("GUI.jCheckBox_memGUIpos", "true")));
        jCheckBox_memGUIdim.setSelected(Boolean.valueOf(props.getProperty("GUI.jCheckBox_memGUIdim", "true")));
        jCheckBox_loadlastDB.setSelected(Boolean.valueOf(props.getProperty("GUI.jCheckBox_loadlastDB", "true")));
        jCheckBox_min2tray.setSelected(Boolean.valueOf(props.getProperty("GUI.jCheckBox_min2tray", "false")));
        createjTreeDBRoot();
        setDatabasesFromProperties();
        setDBtree();
        ToolTipManager.sharedInstance().registerComponent(jTree_DB);
        ToolTipManager.sharedInstance().setInitialDelay(1500);
        ToolTipManager.sharedInstance().setDismissDelay(30000);
        ToolTipManager.sharedInstance().setReshowDelay(15000);
    }    
    
    //Internal Classes
    class CustomTreeCellRenderer extends DefaultTreeCellRenderer {

            public CustomTreeCellRenderer() 
            { 
                super(); 
            } 
            @Override
        public Component getTreeCellRendererComponent(
                                                        JTree tree,
                                                        Object value,
                                                        boolean selection, 
                                                        boolean expanded,
                                                        boolean leaf, 
                                                        int row, 
                                                        boolean hasFocus) { 
                super.getTreeCellRendererComponent(tree, value, selection, expanded, leaf, row, hasFocus); 
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)value; 
                setIconAndToolTip(node.getUserObject(), tree, expanded, leaf, selection);
                if(selection) {
                }
                    
                return(this);
        }
        private void setIconAndToolTip(Object obj, JTree tree, boolean expanded, boolean leaf, boolean selected) {
            setFont((Font) iface.getGUI().getFontMap().get("GA10B"));
            tree.setBackground((Color) iface.getGUI().getColorMap().get("menu"));
            setForeground((Color) iface.getGUI().getColorMap().get("black"));
            setBackgroundNonSelectionColor((Color) iface.getGUI().getColorMap().get("menu"));
            setBackgroundSelectionColor((Color) iface.getGUI().getColorMap().get("header"));
            Icon icon = null;
            if(obj instanceof String) {
                String str = (String)(obj);
                if(str.equals("Database")) {
                    icon = new ImageIcon(iface.getURL("NetExpert.png"));
                    setIcon(icon);
                } else {
                    //Directories
                    boolean isCategory = false;
                    for(int i=0;i < categories.length;i++) {
                        if(str.equals(categories[i])) {
                            isCategory = true;
                            i = categories.length;
                        }
                    }
                    if (isCategory && selected){
                        icon = new ImageIcon(iface.getURL("folder_open.gif"));			
                    } else if(isCategory && expanded){
                        icon = new ImageIcon(iface.getURL("folder_open.gif"));
                    } else if(isCategory) {
                        icon = new ImageIcon(iface.getURL("folder_close.gif"));
                    }
                    if(icon == null) {
                        icon = new ImageIcon(iface.getURL("Database.png"));
                    }
                    setIcon(icon);
                }
                BufferedImage bufferedIcon = null;
                try {
                    bufferedIcon = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_BYTE_INDEXED);
                } catch (Exception ex) {
                    
                }
                Graphics g = bufferedIcon.getGraphics();
                iconPaint(g);
            }            
        }
        @Override
        public Color getBackgroundNonSelectionColor() {
            return super.getBackgroundNonSelectionColor();
        }
        @Override
        public Color getBackgroundSelectionColor() {
            return super.getBackgroundSelectionColor();
        }
        @Override
        public synchronized DropTarget getDropTarget() {
            return super.getDropTarget();
        }
        public int getNEXT() {
            return NEXT;
        }
        public int getPREVIOUS() {
            return PREVIOUS;
        }
        @Override
        public void setBackgroundNonSelectionColor(Color newColor) {
            super.setBackgroundNonSelectionColor(newColor);
        }
        @Override
        public void setBackgroundSelectionColor(Color newColor) {
            super.setBackgroundSelectionColor(newColor);
        }

        @Override
        public void setForeground(Color fg) {
            super.setForeground(fg);
        }

        public void iconPaint( Graphics g ) {
            g.fillRect( 0, 0, getWidth() - 1, getHeight() - 1 );
            g.setColor((Color) iface.getGUI().getColorMap().get("menu"));
            this.paint(g);
        }
    }       
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_DBdel;
    private javax.swing.JButton jButton_DBnew;
    private javax.swing.JButton jButton_DBsave;
    private javax.swing.JCheckBox jCheckBox_loadlastDB;
    private javax.swing.JCheckBox jCheckBox_memGUIdim;
    private javax.swing.JCheckBox jCheckBox_memGUIpos;
    private javax.swing.JCheckBox jCheckBox_min2tray;
    private javax.swing.JComboBox jComboBox_DBcategory;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel_DBcategroy;
    private javax.swing.JLabel jLabel_DBheader;
    private javax.swing.JLabel jLabel_DBpass;
    private javax.swing.JLabel jLabel_DBport;
    private javax.swing.JLabel jLabel_DBserver;
    private javax.swing.JLabel jLabel_DBsid;
    private javax.swing.JLabel jLabel_DBuser;
    private javax.swing.JLabel jLabel_OSI_SYSTEM;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel_DB;
    private javax.swing.JPanel jPanel_DB2;
    private javax.swing.JPasswordField jPasswordField_DBpass;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField_DBport;
    private javax.swing.JTextField jTextField_DBserver;
    private javax.swing.JTextField jTextField_DBsid;
    private javax.swing.JTextField jTextField_DBuser;
    private javax.swing.JTextField jTextField_OSI_SYSTEM;
    private javax.swing.JTree jTree_DB;
    // End of variables declaration//GEN-END:variables

}
