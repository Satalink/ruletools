/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ruletools;

import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;

public interface Pkg_Interface {
    //Package Objects
    
    //Class Interfaces
    public OraDB getDB();
    public GUI getGUI();
    // Main Interfaces
    public void exitApp();
    public void getConnected();
    public boolean getConnStat();
    public ArrayList getDBquery(String sql);    
    public String getOSName();
    public Properties getProperties() throws Exception;
    public void setConnection(Connection connection);
    public void setProperties(Properties props);
    public String getSVNroot();
    public URL getURL(String link);
    public void setDefaultTitle();
}
